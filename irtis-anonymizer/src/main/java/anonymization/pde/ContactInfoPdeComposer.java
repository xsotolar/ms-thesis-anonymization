package anonymization.pde;

import nametag.NerEntity;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Compose Contact info PDE
 */
public class ContactInfoPdeComposer extends PdeComposer {
    /**
     *  Compose PDE of:
     *  TYPE_PHONE_NER, TYPE_PHONE_NUMBER, TYPE_EMAIL
     */
    public ContactInfoPdeComposer() {
        this.pdeType = PdeEntity.TYPE_CONTACT_INFO;
        this.NER_TYPES = new HashSet<>(Arrays.asList(
                NerEntity.TYPE_PHONE_NER,
                NerEntity.TYPE_PHONE_NUMBER,
                NerEntity.TYPE_EMAIL));
    }
}
