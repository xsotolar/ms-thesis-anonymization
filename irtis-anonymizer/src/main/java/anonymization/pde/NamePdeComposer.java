package anonymization.pde;

import nametag.NerEntity;
import nametag.ParsedLine;

import java.util.*;

/**
 * Compose Name PDE
 */
public class NamePdeComposer extends PdeComposer {
    private int minFirstNames;

    /**
     * Compose PDE of:
     * TYPE_FIRST_NAME, TYPE_SECOND_NAME, TYPE_SURNAME, TYPE_SURNAME
     *
     * with QIDs:
     * TYPE_FIRST_NAME, TYPE_SECOND_NAME
     *
     * @param orderCloseness
     * @param indexCloseness
     * @param minIdentifierLength
     * @param minFirstNames
     */
    public NamePdeComposer(int orderCloseness, int indexCloseness, int minIdentifierLength, int minFirstNames) {
        this.orderCloseness = orderCloseness;
        this.indexCloseness = indexCloseness;
        this.minIdentifierLength = minIdentifierLength;
        this.minFirstNames = minFirstNames;
        this.pdeType = PdeEntity.TYPE_NAME;
        this.QID_TYPES = Arrays.asList(
                NerEntity.TYPE_FIRST_NAME,
                NerEntity.TYPE_SECOND_NAME);
        this.NER_TYPES = new HashSet<>(Arrays.asList(
                NerEntity.TYPE_FULL_NAME,
                NerEntity.TYPE_SURNAME,
                NerEntity.TYPE_FIRST_NAME,
                NerEntity.TYPE_SECOND_NAME));
    }

    /**
     * Compose PDe. Scans neighbourhood to the left of the identitfier.
     * @param parsedLine
     * @return
     */
    @Override
    public TreeSet<PdeEntity> compose(ParsedLine parsedLine) {
        NerEntity[] sortedEntities = parsedLine.getSortedEntities(NER_TYPES).toArray(new NerEntity[0]);
        Set<PdeEntity> pdeEntities = new HashSet<>();

        for (int i = 0; i < sortedEntities.length; i++) {
            if (sortedEntities[i].getContent(parsedLine.getLine()).length() < minIdentifierLength) {
                continue;
            }
            // handle full names
            if (NerEntity.TYPE_FULL_NAME.equals(sortedEntities[i].getType())) {
                PdeEntity created = new PdeEntity(sortedEntities[i], PdeEntity.TYPE_NAME);
                created.setTextContent(sortedEntities[i].getContent(parsedLine.getLine()));
                pdeEntities.add(created);
            }
            // handle surnames
            else if (NerEntity.TYPE_SURNAME.equals(sortedEntities[i].getType())) {
                PdeEntity handledSurname = handleSurname(sortedEntities, i, parsedLine);
                if (handledSurname.getQids().size() >= minFirstNames) {
                    pdeEntities.add(handledSurname);
                }
            }
        }
        return removeOverlaps(pdeEntities);
    }

    private PdeEntity handleSurname(NerEntity[] entities, int identifier, ParsedLine parsedLine) {
        PdeEntity result = new PdeEntity(entities[identifier], PdeEntity.TYPE_NAME);
        String surname = entities[identifier].getContent(parsedLine.getLine());
        List<String> firstNames = new ArrayList<>();
        // scan neighborhood
        int i = Math.max(0, identifier - orderCloseness);
        int limit = identifier;//Math.min(entities.length - 1, identifier + orderCloseness);
        int idCenter = (entities[identifier].getEnd() + entities[identifier].getStart()) / 2;

        for (;  i < limit; i++) {
            if (/*i != identifier && */QID_TYPES.contains(entities[i].getType())) {

                int otherCenter = (entities[i].getEnd() + entities[i].getStart()) / 2;
                int distance = Math.abs(otherCenter - idCenter);
                if (distance < indexCloseness) {
                    result.getQids().add(entities[i]);
                    firstNames.add(entities[i].getContent(parsedLine.getLine()));
                }
            }
        }
        firstNames.add(surname);
        result.setTextContent(String.join(" ", firstNames));
        return result;
    }


}
