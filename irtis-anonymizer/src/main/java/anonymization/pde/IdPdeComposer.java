package anonymization.pde;

import nametag.NerEntity;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Compose Id PDE
 */
public class IdPdeComposer extends PdeComposer {
    /**
     * Compose PDE of:
     * TYPE_CARD_PLATE, TYPE_BANK_ACCOUNT, TYPE_CREDIT_NUMBER. TYPE_BIRTH_NUMBER, TYPE_ID_CARD
     */
    public IdPdeComposer() {
        this.pdeType = PdeEntity.TYPE_ID;
        this.NER_TYPES = new HashSet<>(Arrays.asList(
                NerEntity.TYPE_CARD_PLATE,
                NerEntity.TYPE_BANK_ACCOUNT,
                NerEntity.TYPE_CREDIT_NUMBER,
                NerEntity.TYPE_BIRTH_NUMBER,
                NerEntity.TYPE_ID_CARD));
    }


}
