package anonymization.strategies;

import nametag.NerEntity;

import java.util.Random;

/**
 * Anonymize contact info.
 */
public class ContactInfoAnonymizer {
    private static String DEFAULT_VALUE = "CONTACT_INFO";
    private final String MOBILE_BASE = "%s%s %s";
    private final String[] COUNTRY_PREFIX = new String[] { "+420 ", "" };
    private final String[] CZECH_MOBILE_OPRATOR_PREFIX = new String[] {
            "601", "602", "607", "603", "604", "605", "608", "702", "720", "730", "739", "770", "777" };
    private final String MAIL_DOMAIN = "@mail.cz";
    private Random random = new Random();
    private ReplacementGenerator replacementGenerator = new ReplacementGenerator();

    /**
     * Anonymize target according to the NER entity type.
     * @param value target
     * @param nerType NER type
     * @return anonymized value
     */
    public String anonymize(String value, String nerType) {
        String result = DEFAULT_VALUE;
        switch(nerType) {
            case NerEntity.TYPE_PHONE_NER:
            case NerEntity.TYPE_PHONE_NUMBER:
                result = generatePhoneNumber(value);
                break;
            case NerEntity.TYPE_EMAIL:
                result = generateEmail(value);
                break;
            default:
                break;
        }
        return result;
    }

    private String generateEmail(String value) {
        int byHash = replacementGenerator.getIntHash(value, 1000000000);
        return byHash + MAIL_DOMAIN;
    }

    private String generatePhoneNumber(String value) {
        return String.format(MOBILE_BASE,
                COUNTRY_PREFIX[random.nextInt(COUNTRY_PREFIX.length)],
                CZECH_MOBILE_OPRATOR_PREFIX[random.nextInt(CZECH_MOBILE_OPRATOR_PREFIX.length)],
                getPhoneReplacementPart(value));
    }

    private String getPhoneReplacementPart(String phone) {
        int byHash = replacementGenerator.getIntHash(phone, 1000000);
        while (byHash < 100000) {
            byHash *= 10;
        }
        String number = String.valueOf(byHash);
        return number.substring(0,3) + " " + number.substring(3, 6);
    }
}
