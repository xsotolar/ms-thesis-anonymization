package anonymization.strategies;

import anonymization.pde.*;
import gazetteer.exceptions.GazetteerException;
import nametag.NerEntity;
import nametag.NerInMemoryRecognizer;
import nametag.ParsedLine;
import rulematch.RulesMatcher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Anonymize instructured text. Detect 4 PD classes: Name, Id, Location, Contact information.
 */
public class UnstructuredTextAnonymizer implements AnonymizationStrategy {
    private int minInputLength;
    private boolean isUsingRules = true;

    private NerInMemoryRecognizer nerRecognizer = new NerInMemoryRecognizer();
    private RulesMatcher rulesMatcher = new RulesMatcher();
    private PdeComposer namePdeComposer;
    private PdeComposer idPdeComposer = new IdPdeComposer();
    private PdeComposer locationPdeComposer = new LocationPdeComposer();
    private PdeComposer contactInfoPdeComposer = new ContactInfoPdeComposer();

    private FullNameAnonymizer fullNameAnonymizer;
    private IdAnonymizer idAnonymizer;
    private ContactInfoAnonymizer contactInfoAnonymizer;
    private LocationAnonymizer locationAnonymizer;

    /**
     * Constructor. Throws exception if one of the gazetteer datafiles is missing.
     * @throws GazetteerException
     * @throws IOException
     */
    public UnstructuredTextAnonymizer() throws GazetteerException, IOException {
        fullNameAnonymizer = new FullNameAnonymizer();
        idAnonymizer = new IdAnonymizer();
        contactInfoAnonymizer = new ContactInfoAnonymizer();
        locationAnonymizer = new LocationAnonymizer();
    }

    /**
     * Constructor. Throws exception if one of the gazetteer datafiles is missing.
     * @throws GazetteerException
     * @throws IOException
     */
    public UnstructuredTextAnonymizer(FullNameAnonymizer fullNameAnonymizer, IdAnonymizer idAnonymizer,
                                      ContactInfoAnonymizer contactInfoAnonymizer, LocationAnonymizer locationAnonymizer,
                                      int orderCloseness, int indexCloseness, int minIdentifierLength,
                                      int minInputLength, int minFirstNames, boolean isUsingRules) throws GazetteerException  {
        this.minInputLength = minInputLength;
        this.isUsingRules = isUsingRules;
        this.fullNameAnonymizer = fullNameAnonymizer;
        this.idAnonymizer = idAnonymizer;
        this.contactInfoAnonymizer = contactInfoAnonymizer;
        this.locationAnonymizer = locationAnonymizer;
        this.namePdeComposer = new NamePdeComposer(orderCloseness, indexCloseness, minIdentifierLength, minFirstNames);

    }

    /**
     * Anonymize text input.
     * @param input can be multiline.
     * @return anonymized text
     */
    @Override
    public String anonymize(String input) {
        TreeSet<PdeEntity> pdeEntities = extractPdeEntities(input);
        return rebuildAnonymized(input, pdeEntities);
    }

    /**
     * Extract PDE entities from input. Result is linearized to remove overlaps.
     * @param input
     * @return
     */
    public TreeSet<PdeEntity> extractPdeEntities(String input) {
        if (input == null || input.length() < minInputLength) {
            return new TreeSet<>();
        }
        ParsedLine toAnonymize = nerRecognizer.parseLine(input);
        if (isUsingRules) {
            TreeSet<NerEntity> ruleEntities = rulesMatcher.getRuleMatchesByClass(input);
            toAnonymize.getEntities().addAll(ruleEntities);
        }

        TreeSet<PdeEntity> pdeEntities = new TreeSet<>();
        pdeEntities.addAll(namePdeComposer.compose(toAnonymize));
        pdeEntities.addAll(idPdeComposer.compose(toAnonymize));
        pdeEntities.addAll(locationPdeComposer.compose(toAnonymize));
        pdeEntities.addAll(contactInfoPdeComposer.compose(toAnonymize));

        return PdeComposer.linearize(pdeEntities);
    }

    /**
     * Build anonymized result. Overlapping PDE entities are removed.
     * @param input target text
     * @param pdeEntities PDE to anonymize
     * @return anonymized result
     */
    private String rebuildAnonymized(final String input, TreeSet<PdeEntity> pdeEntities) {
        if (pdeEntities.size() < 1) {
            return input;
        }
        ArrayList<String> parts = new ArrayList<>();
        // add anything before first entity
        parts.add(input.substring(0, pdeEntities.first().flatten().getStart()));
        // add entity replacement + string before the next entity
        for (PdeEntity current: pdeEntities) {
            String replacement = getReplacingValue(current, input.substring(current.flatten().getStart(),
                    current.flatten().getEnd()));
            parts.add(replacement);

            PdeEntity next = pdeEntities.higher(current);
            if (next != null) {
                parts.add(input.substring(current.flatten().getEnd(), next.flatten().getStart()));
            } else {
                parts.add(input.substring(current.flatten().getEnd()));
            }
        }
        return String.join("", parts);
    }

    private String getReplacingValue(PdeEntity entity, String original) {
        String result;
        switch (entity.getType()) {
            case PdeEntity.TYPE_NAME:
                result = fullNameAnonymizer.anonymize(original);
                break;
            case PdeEntity.TYPE_ID:
                result = idAnonymizer.anonymize(entity.getNerType());
                break;
            case PdeEntity.TYPE_CONTACT_INFO:
                result = contactInfoAnonymizer.anonymize(original, entity.getNerType());
                break;
            case PdeEntity.TYPE_LOCATION:
                result = locationAnonymizer.anonymize(original, entity.getNerType());
                break;
            default:
                result = entity.getType();
                break;
        }
        return result;
    }
}
