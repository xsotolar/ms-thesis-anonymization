package anonymization.strategies;

import nametag.NerEntity;

import java.util.Random;

/**
 * Anonymize IDs.
 */
public class IdAnonymizer {
    private static String DEFAULT_VALUE = "IDENTIFICATION";
    private Random random = new Random();
    private ReplacementGenerator replacementGenerator = new ReplacementGenerator();

    private final char[] ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVXYZ".toCharArray();

    private final String ACCOUNT_BASE = "%s/%s";
    private final String CREDIT_BASE = "%s %s %s %s";
    private final String BIRTH_NUMBER_BASE = "%s/%s";
    private final String ID_BASE = "%s";

    /**
     * Anonymize target according to the NER entity type.
     * @param nerType NER type
     * @return anonymized value
     */
    public String anonymize(String nerType) {
        String result = DEFAULT_VALUE;
        switch(nerType) {
            case NerEntity.TYPE_CARD_PLATE:
                result = generateCarPlate();
                break;
            case NerEntity.TYPE_BANK_ACCOUNT:
                result = generateBankAccount();
                break;
            case NerEntity.TYPE_CREDIT_NUMBER:
                result = generateCreditNumber();
                break;
            case NerEntity.TYPE_BIRTH_NUMBER:
                result = generateBirthNumber();
                break;
            case NerEntity.TYPE_ID_CARD:
                result = generateIdCardNumber();
                break;
            default:
                break;
        }
        return result;
    }

    private String generateCarPlate() {
        StringBuilder sb = new StringBuilder();
        sb.append(random.nextInt(10));
        sb.append(ALPHABET[random.nextInt(ALPHABET.length)]);
        sb.append(random.nextInt(10));
        sb.append(" ");
        sb.append(replacementGenerator.getRandomInt(1000, 9999));
        return sb.toString();
    }

    private String generateBankAccount() {
        return String.format(ACCOUNT_BASE, replacementGenerator.generateRandomNumericalString(10),
                replacementGenerator.generateRandomNumericalString(4));
    }

    private String generateCreditNumber() {
        return String.format(CREDIT_BASE,
                replacementGenerator.generateRandomNumericalString(4),
                replacementGenerator.generateRandomNumericalString(4),
                replacementGenerator.generateRandomNumericalString(4),
                replacementGenerator.generateRandomNumericalString(4));
    }

    private String generateBirthNumber() {
        return String.format(BIRTH_NUMBER_BASE,
                replacementGenerator.generateRandomNumericalString(6),
                replacementGenerator.generateRandomNumericalString(4));
    }

    private String generateIdCardNumber() {
        return String.format(ID_BASE,
                replacementGenerator.generateRandomNumericalString(10));
    }




}
