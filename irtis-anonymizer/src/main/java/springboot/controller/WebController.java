package springboot.controller;

import anonymization.pde.PdeEntity;
import html.HtmlGenerator;
import nametag.NerEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import server.entities.GeneralText;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static springboot.WebApplication.WEB_ANONYMIZER;

/**
 * Demo web app controller.
 */
@Controller
public class WebController {

    private HtmlGenerator htmlGenerator = new HtmlGenerator();

    /**
     * Handle anonymize requests.
     * @param input
     * @param showFiles
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value="/", method=RequestMethod.POST, params="action=anonymize")
    public String handleAnonymization(
            @RequestParam("file") String input,
            @RequestParam(value="showFiles", required = true, defaultValue = "false") boolean showFiles,
            RedirectAttributes redirectAttributes) {

        if (input == null || input.length() < 1) {
            return "redirect:/";
        }
        String[] split = input.split("\n");
        GeneralText data = new GeneralText();
        List<String> inputs = new ArrayList<>();
        for (String s: split) {
            data.setText(s);
            WEB_ANONYMIZER.anonymize(data);
            inputs.add(data.getText());
        }
        redirectAttributes.addFlashAttribute("message", input);
        redirectAttributes.addFlashAttribute("files", inputs);
        redirectAttributes.addFlashAttribute("showFiles", true);

        return "redirect:/";
    }

    /**
     * Handle tagger requests.
     * @param input
     * @param showFiles
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value="/", method=RequestMethod.POST, params="action=tag")
    public String handleTagging(
            @RequestParam("file") String input,
            @RequestParam(value="showFiles", required = true, defaultValue = "false") boolean showFiles,
            RedirectAttributes redirectAttributes) {

        if (input == null || input.length() < 1) {
            return "redirect:/";
        }
        List<String> inputs = new ArrayList<>();
        TreeSet<PdeEntity> pdeEntities = WEB_ANONYMIZER.getUnstructuredTextAnonymizer().extractPdeEntities(input);
        List<NerEntity> flatPdes = pdeEntities.stream().map(PdeEntity::flatten).collect(Collectors.toList());
        String colored = htmlGenerator.getColoredPde(flatPdes, input + " ");
        inputs.add(colored);

        redirectAttributes.addFlashAttribute("message", input);
        redirectAttributes.addFlashAttribute("files", inputs);
        redirectAttributes.addFlashAttribute("showFiles", true);

        return "redirect:/";
    }

    /**
     * Handle index request.
     * @return
     */
    @GetMapping("/")
    public String index() {
        return "inputForm";
    }

}