package springboot;

import anonymization.CompositeDataAnonymizer;
import gazetteer.exceptions.GazetteerException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import java.io.IOException;

/**
 * Demo web application.
 */
@SpringBootApplication(exclude={SecurityAutoConfiguration.class})
public class WebApplication {
    public static CompositeDataAnonymizer WEB_ANONYMIZER;

    static int orderCloseness = 1;
    static int indexCloseness = 30;
    static int minIdentifierLength = 5;
    static int minInputLength = 4;
    static int minFirstNames = 0;
    static boolean isUsingRules = true;

    /**
     * Run web app.
     * @param args
     * @throws IOException
     * @throws GazetteerException
     */
    public static void main(String[] args) throws IOException, GazetteerException {
        System.out.println("Loading language models...");
        WEB_ANONYMIZER = new CompositeDataAnonymizer(orderCloseness, indexCloseness, minIdentifierLength, minInputLength, minFirstNames, isUsingRules);
        System.out.println("Loading done.");
        SpringApplication.run(WebApplication.class, args);
    }

}

