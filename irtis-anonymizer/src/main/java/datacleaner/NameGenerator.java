package datacleaner;

import java.util.HashSet;
import java.util.Random;

/**
 * Create synthetic PDE gazetteer.
 */
public class NameGenerator {
    private Random random = new Random();

    public HashSet<String> maleNames;
    public HashSet<String> femaleNames;

    private final String[] firstNames_male = {"Jiří", "Jan", "Petr", "Josef", "Pavel", "Martin", "Jaroslav", "Tomáš", "Miroslav", "Zdeněk", "František", "Václav", "Michal", "Milan", "Karel", "Jakub", "Lukáš", "David", "Vladimír", "Ladislav", "Ondřej", "Roman", "Stanislav", "Marek", "Radek", "Daniel", "Antonín", "Vojtěch", "Filip", "Adam" };
    private final String[] firstNames_female = {"Marie", "Jana", "Eva", "Hana", "Anna", "Lenka", "Kateřina", "Věra", "Lucie", "Alena", "Petra", "Jaroslava", "Veronika", "Martina", "Jitka", "Tereza", "Ludmila", "Helena", "Michaela", "Zdeňka", "Ivana", "Jarmila", "Monika", "Zuzana", "Jiřina", "Markéta", "Eliška", "Marcela", "Barbora", "Dagmar"};
    private final String[] surnames_male = {"Novák", "Svoboda", "Novotný", "Dvořák", "Černý", "Procházka", "Kučera", "Veselý", "Horák", "Němec", "Pokorný", "Marek", "Pospíšil", "Hájek", "Jelínek", "Král", "Růžička", "Beneš", "Fiala", "Sedláček", "Doležal", "Zeman", "Nguyen", "Nguyen Thi", "Nguyenová", "Kolář", "Krejčí", "Navrátil", "Čermák", "Urban", "Vaněk", "Blažek"};
    private final String[] surnames_female = {"Nováková", "Svobodová", "Novotná", "Dvořáková", "Černá", "Procházková", "Kučerová", "Veselá", "Horáková", "Němcová", "Pokorná", "Marková", "Pospíšilová", "Hájková", "Jelínková", "Králová", "Růžičková", "Benešová", "Fialová", "Sedláčková", "Doležalová", "Zemanová", "Nguyen", "Kolářová", "Krejčová", "Navrátilová", "Čermáková", "Urbanová", "Vaňková", "Blažková"};

    /**
     * Initializes the gazetteers.
     */
    public NameGenerator() {
        this.maleNames =  new HashSet<>(firstNames_male.length * surnames_male.length);
        this.femaleNames =  new HashSet<>(firstNames_female.length * surnames_female.length);
        initGazetteers();
    }

    private void initGazetteers() {
        for (String firstName: firstNames_male) {
            for (String surname: surnames_male) {
                maleNames.add(firstName + " " + surname);
            }
        }
        for (String firstName: firstNames_female) {
            for (String surname: surnames_female) {
                femaleNames.add(firstName + " " + surname);
            }
        }
    }

    /**
     * Check if the gazetteer contains an exact value.
     * @param value
     * @return
     */
    public boolean contains(String value) {
        return maleNames.contains(value) || femaleNames.contains(value);
    }
}
