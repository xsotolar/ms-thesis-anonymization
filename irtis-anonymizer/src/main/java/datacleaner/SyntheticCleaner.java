package datacleaner;

public class SyntheticCleaner {
    MobileNumberGenerator mobileNumberGenerator = new MobileNumberGenerator();
    NameGenerator nameGenerator = new NameGenerator();

    /**
     * Check if the gazetteers contains an exact value.
     * @param value
     * @return
     */
    public boolean contains(String value) {
        return nameGenerator.contains(value) || mobileNumberGenerator.contains(value);
    }
}
