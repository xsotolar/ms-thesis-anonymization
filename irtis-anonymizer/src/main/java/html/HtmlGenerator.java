package html;

import brat.BratEntity;
import nametag.EntityMatcher;
import nametag.NerEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Create HTML output.
 */
public class HtmlGenerator {
    private String nerColor = "#fff780";
    private String piColor = "#a6e6ff";
    private String overlapColor = "#a6ffa7";

    private final String template = "<!DOCTYPE html>" +
            "<html lang=\"en\">" +
            "  <head>" +
            "    <meta charset=\"utf-8\">" +
            "    <title>Personal information recognition results</title>" +
            "    <style>" +
            "      body {" +
            "          font-family: Verdana, Arial, sans-serif;" +
            "      }" +
            "      /* Tooltip container */" +
            "      .tooltip {" +
            "        position: relative;" +
            "        display: inline-block;" +
            "      }" +
            "      " +
            "      /* Tooltip text */" +
            "      .tooltip .tooltiptext {" +
            "        visibility: hidden;" +
            "        width: 400px;" +
            "        background-color: black;" +
            "        color: #fff;" +
            "        text-align: center;" +
            "        padding: 5px 0;" +
            "        border-radius: 6px;" +
            "       " +
            "        /* Position the tooltip text - see examples below! */" +
            "        position: absolute;" +
            "        z-index: 1;" +
            "      }" +
            "      " +
            "      /* Show the tooltip text when you mouse over the tooltip container */" +
            "      .tooltip:hover .tooltiptext {" +
            "        visibility: visible;" +
            "      }" +
            "    </style>" +
            "  </head>" +
            "  <body>" +
            "  <H1>Personal information recognition results</H1>" +
            "    <p><b>File name: </b>%s</p>" +
            "    <div class=\"tooltip\"><b>Precision: </b>%.2f" +
            "      <span class=\"tooltiptext\">The number of correctly identified named anonymization.entities, averaged with the number of named anonymization.entities retrieved by the system</span>" +
            "    </div><br/>" +
            "    <div class=\"tooltip\"><b>Recall: </b>%.2f" +
            "      <span class=\"tooltiptext\">The number of correctly identified named anonymization.entities, divided by the number of correct (gold) named anonymization.entities</span>" +
            "    </div><br/>" +
            "    <div class=\"tooltip\"><b>F1-measure: </b>%.2f" +
            "      <span class=\"tooltiptext\">The harmonic mean of precision and recall</span>" +
            "    </div>" +
            "    <p><b>Legend: </b><span style=\"background-color: %s\">Personal Information anonymization.entities</span> <span style=\"background-color: %s\">NER anonymization.entities</span> <span style=\"background-color: %s\">PI, NER overlap</span></p>" +
            "    <H3>File content</H3>" +
            "    <p>%s</p>" +
            "  </body>" +
            "</html>";

    /**
     * Get HTML snippet with colored PDE
     * @param nerEntities
     * @param plaintext
     * @return
     */
    public String getColoredPde(List<NerEntity> nerEntities, String plaintext) {
        List<ColorPosition> spans = getSpansPde(nerEntities, plaintext);
        String htmlBody = getHtmlBody(spans, plaintext);
        return String.format("%s", htmlBody);
    }

    /**
     * Get single file HTML result page
     * @param bratEntities
     * @param nerEntities
     * @param plaintext
     * @param filename
     * @param precision
     * @param recall
     * @param fmeasure
     * @return
     */
    public String getHtmlResultPage(List<BratEntity> bratEntities, List<NerEntity> nerEntities, String plaintext, String filename, float precision, float recall, float fmeasure) {
        List<ColorPosition> spans = getSpans(bratEntities, nerEntities, plaintext);
        String htmlBody = getHtmlBody(spans, plaintext);
        return String.format(template, filename, precision, recall, fmeasure, nerColor, piColor, overlapColor, htmlBody);
    }

    private String getHtmlBody(List<ColorPosition> spans, String text) {
        StringBuilder result = new StringBuilder();

        int prevSpandEnd = 0;
        for (int i = 0; i < spans.size(); i++) {
            ColorPosition s = spans.get(i);
            result.append(text.substring(prevSpandEnd, s.start));
            result.append(getStyleHtml(s.color));
            result.append(text.substring(s.start, s.end));
            result.append("</span>");
            prevSpandEnd = s.end;
        }
        result.append(text.substring(prevSpandEnd));

        return result.toString().replaceAll("\\n", "<br/>");
    }

    private String getStyleHtml(int color) {
        String result;
        switch (color) {
            case 1:
                result = "<span style=\"background-color: #fff780\">";
                break;
            case 10:
                result = "<span style=\"background-color: #a6e6ff\">";
                break;
            case 11:
            case 21:
                result = "<span style=\"background-color: #a6ffa7\">";
                break;
            default:
                result = "<span style=\"background-color: #f54242\">";
                break;
                //throw new RuntimeException("Invalid color code: " + color);
        }
        return result;
    }

    private List<ColorPosition> getSpans(List<BratEntity> bratEntities, List<NerEntity> nerEntities, String text) {
        // spill colors
        int[] colors = new int[text.length()];
        for (BratEntity be : bratEntities) {
            for (int i = be.getStartIndex(); i < be.getEndIndex(); i++) {
                 colors[i]++;
            }
        }
        for (NerEntity ne : nerEntities) {
//            if (entityMatcher.isAllowedNerClass(ne.getType())) {
                for (int i = ne.getStart(); i < ne.getEnd(); i++) {
                    colors[i] += 10;
//                }
            }
        }
        // collect spans
        List<ColorPosition> result = new ArrayList<>();
        ColorPosition colorPosition = null;
        for (int i = 0; i < colors.length; i++) {
            if (i == colors.length - 1 && colorPosition != null) { // end of text
                colorPosition.end = i;
                result.add(colorPosition);
                break;
            }
            if (colors[i] == 0) { // no color
                if (colorPosition != null) {
                    colorPosition.end = i;
                    result.add(colorPosition);
                    colorPosition = null;
                }
            } else if (colorPosition == null) { // new color discovered
                colorPosition = new ColorPosition(colors[i], i);
            } else if (colors[i] != colorPosition.color) { // two colors touch
                colorPosition.end = i;
                result.add(colorPosition);
                colorPosition = new ColorPosition(colors[i], i);
            }
        }
        return result;
    }

    private List<ColorPosition> getSpansPde(List<NerEntity> nerEntities, String text) {
        // spill colors
        int[] colors = new int[text.length()];
        for (NerEntity ne : nerEntities) {
            for (int i = ne.getStart(); i < ne.getEnd(); i++) {
                    colors[i] += 10;
            }
        }
        // collect spans
        List<ColorPosition> result = new ArrayList<>();
        ColorPosition colorPosition = null;
        for (int i = 0; i < colors.length; i++) {
            if (i == colors.length - 1 && colorPosition != null) { // end of text
                colorPosition.end = i;
                result.add(colorPosition);
                break;
            }
            if (colors[i] == 0) { // no color
                if (colorPosition != null) {
                    colorPosition.end = i;
                    result.add(colorPosition);
                    colorPosition = null;
                }
            } else if (colorPosition == null) { // new color discovered
                colorPosition = new ColorPosition(colors[i], i);
            } else if (colors[i] != colorPosition.color) { // two colors touch
                colorPosition.end = i;
                result.add(colorPosition);
                colorPosition = new ColorPosition(colors[i], i);
            }
        }
        return result;
    }

    private class ColorPosition {
        public ColorPosition(int color, int start) {
            this.color = color;
            this.start = start;
        }
        int color;
        int start;
        int end;
    }
}
