package brat;

/**
 * POJO for Brat entity
 */
public class BratEntity {
    private String type;
    private int startIndex;
    private int endIndex;
    private String text;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    /**
     * Checks if input is <= start
     * @param endIndex
     */
    public void setEndIndex(int endIndex) {
        if (endIndex <= startIndex) {
            throw new IllegalArgumentException("invalid interval: [" + startIndex + "," + endIndex + "]");
        }
        this.endIndex = endIndex;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getTextLength() {
        return endIndex - startIndex;
    }

    public int getLengthAlphaNumOnly() {
        return text.replaceAll("[^A-Za-z0-9]", "").length();
    }
}
