import anonymization.CompositeDataAnonymizer;
import gazetteer.exceptions.GazetteerException;
import server.entities.GeneralText;

import java.io.*;

/**
 * Contains runnable main method.
 * Example command: gradlew run --args="'first input' 'another input'"
 */
public class Main {

    /**
     * Anonymizes a list of text inputs.
     * @param args text inputs
     * @throws IOException
     * @throws GazetteerException
     */
    public static void main(String... args) throws IOException, GazetteerException {
        int orderCloseness = 1;
        int indexCloseness = 30;
        int minIdentifierLength = 5;
        int minInputLength = 4;
        int minFirstNames = 0;
        boolean isUsingRules = true;

        System.out.println("Loading language models...");
        CompositeDataAnonymizer anonymizer = new CompositeDataAnonymizer(orderCloseness, indexCloseness,
                minIdentifierLength, minInputLength, minFirstNames, isUsingRules);
        System.out.println("Loading done.");

        System.out.println("------Anonymized:--------");
        GeneralText data = new GeneralText();
        for (String s: args) {
            data.setText(s);
            anonymizer.anonymize(data);
            System.out.println(data.getText());
        }
    }

}
