package messenger.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * POJO for json mapping
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Conversation {
    private List<Participant> participants;
    private List<Message> messages;
    private String title;
    private String thread_path;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Message m : getMessages()) {
            sb.append(m.getContent());
            sb.append("\n");
        }
        return sb.toString();
    }

    public List<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThread_path() {
        return thread_path;
    }

    public void setThread_path(String thread_path) {
        this.thread_path = thread_path;
    }
}
