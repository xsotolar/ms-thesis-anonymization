package messenger.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * POJO for json mapping
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Participant {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
