package rulematch;

import nametag.NerEntity;
import nametag.NerScanResult;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Run NER by rules.
 */
public class RulesMatcher {
    private String opNumber = "\\b[\\d]{10}\\b";
    private Pattern opNumberRegex = Pattern.compile(opNumber);

    private String birthNumber = "\\b[\\d]{6}/[\\d]{4}\\b";
    private Pattern birthNumberRegex = Pattern.compile(birthNumber);

    private String cardNumber = "\\b[\\d]{4}.?[\\d]{4}.?[\\d]{4}.?[\\d]{4}\\b";
    private Pattern cardNumberRegex = Pattern.compile(cardNumber);

    private String accountNumber = "\\b[\\d]{6,12}/[\\d]{3,4}\\b";
    private Pattern accountNumberRegex = Pattern.compile(accountNumber);

    private String phoneNumber = "[+]?\\d[\\s\\d]{7,15}\\d\\b";
    private Pattern phoneNumberRegex = Pattern.compile(phoneNumber);

    private String spz = "\\b\\d[A-Z]{1}[\\d,A-Z]{1}.?[\\d]{4,5}\\b";
    private Pattern spzRegex = Pattern.compile(spz);

    private String email = "\\b[-a-z0-9~!$%^&*_=+}{\\'?]+(\\.[-a-z0-9~!$%^&*_=+}{\\'?]+)*@([a-z0-9_][-a-z0-9_]*(\\.[-a-z0-9_]+)*\\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}))(:[0-9]{1,5})?\\b";
    private Pattern emailRegex = Pattern.compile(email);

    private String gps = "\\b\\d{1,3}°\\d{1,3}’\\d{1,3}\\.\\d{1,3}.{1,3}N.{1,3}\\d{1,3}°\\d{1,3}’\\d{1,3}\\.\\d{1,3}.{1,3}E\\b|\\b\\d{1,3}\\s{1,3}\\d{1,3}\\.\\d{3,6}.{1,3}\\b\\d{1,3}\\s{1,3}\\d{1,3}\\.\\d{3,6}\\b|\\b\\d{1,3}\\.\\d{3,6}.{1,3}\\d{1,3}\\.\\d{3,6}\\b";
    private Pattern gpsRegex = Pattern.compile(gps);

    private String ipv4Address = "\\b\\d+\\.\\d+\\.\\d+\\.\\d+\\b";
    private Pattern ipv4AddressRegex = Pattern.compile(ipv4Address);

    /**
     * Get rule matches labeled by its type.
     *
     * Note: performance is fine despite multiple regex pass though of the input.
     *
     * @param text
     * @return
     */
    public TreeSet<NerEntity> getRuleMatchesByClass(String text) {
        List<NerEntity> all = new ArrayList<>();
        all.addAll(getMatches(spzRegex.matcher(text), text, NerEntity.TYPE_CARD_PLATE));
        all.addAll(getMatches(opNumberRegex.matcher(text), text, NerEntity.TYPE_ID_CARD));
        all.addAll(getMatches(birthNumberRegex.matcher(text), text, NerEntity.TYPE_BIRTH_NUMBER));
        all.addAll(getMatches(cardNumberRegex.matcher(text), text, NerEntity.TYPE_CREDIT_NUMBER));
        all.addAll(getMatches(accountNumberRegex.matcher(text), text, NerEntity.TYPE_BANK_ACCOUNT));
        all.addAll(getMatches(phoneNumberRegex.matcher(text), text, NerEntity.TYPE_PHONE_NUMBER));
        all.addAll(getMatches(emailRegex.matcher(text), text, NerEntity.TYPE_EMAIL));
        all.addAll(getMatches(ipv4AddressRegex.matcher(text), text, NerEntity.TYPE_IPV4));
        all.addAll(getMatches(gpsRegex.matcher(text), text, NerEntity.TYPE_GPS));

        return removeOverlaps(all);
    }

    /**
     * Remove any entity overlaps. First entity has precedence.
     * @param entities with overlaps
     * @return without overlaps
     */
    public TreeSet<NerEntity> removeOverlaps(List<NerEntity> entities) {
        if (entities == null || entities.size() < 1) {
            return new TreeSet<>();
        }
        TreeSet<NerEntity> sorted = new TreeSet<>(entities);
        TreeSet<NerEntity> clean = new TreeSet<>();

        NerEntity current = sorted.first();
        clean.add(current);
        while (current != null) {
            current = sorted.higher(current);
            if (current == null) {
                break;
            }
            if (current.getStart() >= clean.last().getEnd()) {
                clean.add(current);
            }
        }
        return clean;
    }

    private List<NerEntity> getMatches(Matcher matcher, String text, String neType) {
        List<NerEntity> entities = new ArrayList<>();
        while (matcher.find()) {
            entities.add(new NerEntity(matcher.start(), matcher.end(), neType));
        }
        return entities;
    }



}
