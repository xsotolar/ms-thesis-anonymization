package server.entities;

import java.util.List;

/**
 * POJO that maps to corresponding database entity.
 */
public class Conversation {
    private MetricIdentityEntity metricIdentity;
    private String from;
    private List<Message> messages;

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
