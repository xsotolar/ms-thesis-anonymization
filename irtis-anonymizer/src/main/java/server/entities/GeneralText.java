package server.entities;

/**
 * POJO that maps to corresponding database entity.
 */
public class GeneralText {
    private MetricIdentityEntity metricIdentity;
    private String text;

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
