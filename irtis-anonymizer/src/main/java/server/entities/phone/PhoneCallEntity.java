package server.entities.phone;

import server.entities.MetricEntity;
import server.entities.MetricIdentityEntity;

/**
 * POJO that maps to corresponding database entity.
 */
public class PhoneCallEntity implements MetricEntity {
    private MetricIdentityEntity metricIdentity;
    private String name;
    private String phoneNumber;
    private String type;
    private int duration;
    private Long callDate;

    public PhoneCallEntity() {
    }

    public PhoneCallEntity(MetricIdentityEntity metricIdentity, String name, String phoneNumber, String type, int duration, Long callDate) {
        this.metricIdentity = metricIdentity;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.duration = duration;
        this.callDate = callDate;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getType() {
        return type;
    }

    public int getDuration() {
        return duration;
    }

    public Long getCallDate() {
        return callDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
