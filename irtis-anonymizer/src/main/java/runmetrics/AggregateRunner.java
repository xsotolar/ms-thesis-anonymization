package runmetrics;

import anonymization.CompositeDataAnonymizer;
import anonymization.pde.PdeEntity;
import anonymization.strategies.UnstructuredTextAnonymizer;
import brat.BratEntity;
import brat.StandoffFile;
import brat.StandoffReader;
import datacleaner.SyntheticCleaner;
import gazetteer.exceptions.GazetteerException;
import html.HtmlGenerator;
import nametag.*;
import rulematch.RulesMatcher;
import server.entities.GeneralText;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Run metrics on all annotated files in the 'testdata/students/<subfolder>/' dirs.
 * This class is not a part of the module.
 */
public class AggregateRunner {
    private StandoffReader standoffReader = new StandoffReader();
    private EntityMatcher entityMatcher = new EntityMatcher();
    private HtmlGenerator htmlGenerator = new HtmlGenerator();
    private RulesMatcher rulesMatcher = new RulesMatcher();
    private SyntheticCleaner syntheticCleaner = new SyntheticCleaner();

    /**
     * Get anonymization throughput on all annotation source files.
     * @return
     * @throws IOException
     * @throws GazetteerException
     */
    public String measureAnonThroughput() throws IOException, GazetteerException {
        int orderCloseness = 1;
        int indexCloseness = 30;
        int minIdentifierLength = 5;
        int minInputLength = 4;
        int minFirstNames = 0;
        boolean isUsingRules = true;

        CompositeDataAnonymizer anonymizer = new CompositeDataAnonymizer(orderCloseness, indexCloseness, minIdentifierLength, minInputLength, minFirstNames, isUsingRules);

        long startTime = System.nanoTime();
        Path studentData = Paths.get("testdata", "students");
        List<Path> studentFolders = Files
                .walk(studentData, 1)
                .filter(f -> !studentData.equals(f))
                .filter(Files::isDirectory)
                .collect(Collectors.toList());

        for (Path studentFolder : studentFolders) {
            List<Path> annFiles = Files.walk(studentFolder, 1)
                    .filter(f -> !studentFolder.equals(f))
                    .filter(Files::isRegularFile)
                    .filter(f -> f.toString().endsWith(".txt"))
                    .collect(Collectors.toList());

            for (Path ann : annFiles) {
                GeneralText data = new GeneralText();
                data.setText(Files.readString(ann));
                anonymizer.anonymize(data);
            }
        }
        long elapsedTime = System.nanoTime() - startTime;
        return String.valueOf("Elapsed seconds: " + elapsedTime/(1000000*1000));
    }

    /**
     * Calculate metrics on natural PDE.
     * @return
     * @throws IOException
     * @throws GazetteerException
     */
    public String runOnStudents() throws IOException, GazetteerException {
        List<PdeEntity> retrieved = new ArrayList<>();
        List<PdeEntity> hits = new ArrayList<>();
        List<PdeEntity> misses = new ArrayList<>();
        List<BratEntity> gold = new ArrayList<>();
        float totalRecall = 0f;
        int scanedCount = 0;

        int orderCloseness = 1; int indexCloseness = 30; int minIdentifierLength = 5; int minInputLength = 4;
        int minFirstNames = 0; boolean isUsingRules = true;

        UnstructuredTextAnonymizer unstructuredTextAnonymizer = new UnstructuredTextAnonymizer(null, null, null, null,
                orderCloseness, indexCloseness, minIdentifierLength, minInputLength, minFirstNames, isUsingRules);

        Path studentData = Paths.get("testdata", "students");
        List<Path> studentFolders = Files
                .walk(studentData, 1)
                .filter(f -> !studentData.equals(f))
                .filter(Files::isDirectory)
                .collect(Collectors.toList());

        for (Path studentFolder : studentFolders) {
            List<Path> annFiles = Files.walk(studentFolder, 1)
                    .filter(f -> !studentFolder.equals(f))
                    .filter(Files::isRegularFile)
                    .filter(f -> f.toString().endsWith(".ann"))
                    .collect(Collectors.toList());

            for (Path ann : annFiles) {
                AnnotationResult annResult = processAnnFilePdeMatch(ann, unstructuredTextAnonymizer);
                retrieved.addAll(annResult.getRetrieved());
                hits.addAll(annResult.getHits());
                misses.addAll(annResult.getMisses());
                gold.addAll(annResult.getGold());
                totalRecall += entityMatcher.calculateRecall(annResult.getRecall(), annResult.getGold().size());
                scanedCount++;
            }
        }

        StringBuilder sb = new StringBuilder();
        addEndRule(sb);
        addSection("Metrics", sb);
        sb.append("Scanned files (wo empty): " + scanedCount + "\n");
        sb.append("Total PDE entities: " + retrieved.size() + "\n");
        float prec = entityMatcher.calculateUnweightedPrecisison(hits.size(), retrieved.size());
        sb.append("Total precision: " + prec + "\n");
        float recl = entityMatcher.calculateUnweightedRecall(totalRecall, gold.size());
        sb.append("Total recall: " + recl + "\n");
        sb.append("Total fmeasure: " + entityMatcher.calculateFmeasure(prec, recl) + "\n");
        addEndRule(sb);

        addSection("Gold (Brat) entities", gold.size(), sb);
        gold.stream().collect(Collectors.groupingBy(BratEntity::getType, Collectors.counting()))
                .forEach((k,v) -> sb.append(k + ": " + v + "\n"));
        addEndRule(sb);

        addSection("Retrieved PDE", retrieved.size(), sb);
        retrieved.stream().collect(Collectors.groupingBy(PdeEntity::getType, Collectors.counting()))
                .forEach((k,v) -> sb.append(k + ": " + v + "\n"));
        addEndRule(sb);

        addSection("True positive PDE", hits.size(), sb);
        hits.stream().collect(Collectors.groupingBy(PdeEntity::getType, Collectors.counting()))
                .forEach((k,v) -> sb.append(k + ": " + v + "\n"));
        addEndRule(sb);

        addSection("False positive PDE", misses.size(), sb);
        misses.stream().collect(Collectors.groupingBy(PdeEntity::getType, Collectors.counting()))
                .forEach((k,v) -> sb.append(k + ": " + v + "\n"));
        addEndRule(sb);

        return sb.toString();
    }

    /**
     * Calculate annotator performance on synthetic PDE.
     * @return
     * @throws IOException
     * @throws GazetteerException
     */
    public String runOnStudentsBrat() throws IOException, GazetteerException {
        List<BratEntity> retrieved = new ArrayList<>();
        List<BratEntity> hits = new ArrayList<>();
        List<PdeEntity> gold = new ArrayList<>();
        float totalRecall = 0f;
        int scanedCount = 0;

        int orderCloseness = 1;
        int indexCloseness = 30;
        int minIdentifierLength = 5;
        int minInputLength = 4;
        int minFirstNames = 0;
        boolean isUsingRules = true;

        UnstructuredTextAnonymizer unstructuredTextAnonymizer = new UnstructuredTextAnonymizer(null, null, null, null,
                orderCloseness, indexCloseness, minIdentifierLength, minInputLength, minFirstNames, isUsingRules);

        Path studentData = Paths.get("testdata", "students");
        List<Path> studentFolders = Files
                .walk(studentData, 1)
                .filter(f -> !studentData.equals(f))
                .filter(Files::isDirectory)
                .collect(Collectors.toList());

        for (Path studentFolder : studentFolders) {
            List<Path> annFiles = Files.walk(studentFolder, 1)
                    .filter(f -> !studentFolder.equals(f))
                    .filter(Files::isRegularFile)
                    .filter(f -> f.toString().endsWith(".ann"))
                    .collect(Collectors.toList());

            for (Path ann : annFiles) {
                AnnotationResult annResult = processAnnFilePdeMatch_Annotators(ann, unstructuredTextAnonymizer);
                retrieved.addAll(annResult.retrieved_brat);
                hits.addAll(annResult.hits_brat);
                gold.addAll(annResult.gold_brat);
                totalRecall += entityMatcher.calculateRecall(annResult.getRecall(), annResult.gold_brat.size());
                if (annResult.gold_brat.size() > 0)  {
                    scanedCount++;
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        addEndRule(sb);
        addSection("Metrics", sb);
        sb.append("Scanned files (wo empty): " + scanedCount + "\n");
        sb.append("Total PDE entities: " + retrieved.size() + "\n");
        float prec = entityMatcher.calculateUnweightedPrecisison(hits.size(), retrieved.size());
        sb.append("Total precision: " + prec + "\n");
        float recl = entityMatcher.calculateUnweightedRecall(totalRecall, gold.size());
        sb.append("Total recall: " + recl + "\n");
        sb.append("Total fmeasure: " + entityMatcher.calculateFmeasure(prec, recl) + "\n");
        addEndRule(sb);
        return sb.toString();
    }

    /**
     * Calculate corpus composition.
     * @return
     * @throws IOException
     * @throws GazetteerException
     */
    public String getEntityStatistics() throws IOException, GazetteerException {
        int orderCloseness = 1; int indexCloseness = 30; int minIdentifierLength = 5; int minInputLength = 4;
        int minFirstNames = 0; boolean isUsingRules = true;

        int cbrat = 0; int sbrat = 0; int canon = 0; int sanon = 0;

        UnstructuredTextAnonymizer unstructuredTextAnonymizer = new UnstructuredTextAnonymizer(null, null, null, null,
                orderCloseness, indexCloseness, minIdentifierLength, minInputLength, minFirstNames, isUsingRules);

        Path studentData = Paths.get("testdata", "students");
        List<Path> studentFolders = Files
                .walk(studentData, 1)
                .filter(f -> !studentData.equals(f))
                .filter(Files::isDirectory)
                .collect(Collectors.toList());

        for (Path studentFolder : studentFolders) {
            List<Path> annFiles = Files.walk(studentFolder, 1)
                    .filter(f -> !studentFolder.equals(f))
                    .filter(Files::isRegularFile)
                    .filter(f -> f.toString().endsWith(".ann"))
                    .collect(Collectors.toList());

            for (Path ann : annFiles) {
                int[] annResult = countAnnotations(ann, unstructuredTextAnonymizer);
                cbrat += annResult[0];
                sbrat += annResult[1];
                canon += annResult[2];
                sanon += annResult[3];
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Actual Brat entities: " + cbrat + "\n");
        sb.append("Synthetic Brat entities: " + sbrat + "\n");
        sb.append("Actual PDE entities: " + canon + "\n");
        sb.append("Synthetic PDE entities: " + sanon + "\n");
        sb.append("Total Brat: " + (cbrat + sbrat) + "\n");
        sb.append("Total PDE: " + (canon + sanon) + "\n");
        return sb.toString();
    }

    private AnnotationResult processAnnFilePdeMatch(Path ann, UnstructuredTextAnonymizer unstructuredTextAnonymizer) throws IOException {
        final Path plaintext =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".txt"));
        final Path html_result =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".html"));

        String plaintextContent = Files.readString(plaintext).replaceAll("\\r\\n", "\n");

        // get Brat entities
        StandoffFile standoffFile = standoffReader.extractBratEntities(ann, "");
        Map<Boolean, List<BratEntity>> collectBrat = standoffFile.getEntities().stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getText())));
        List<BratEntity> bratEntities  = collectBrat.get(false);
        List<BratEntity> bratEntities_synthetic  = collectBrat.get(true);

        // get PDEs
        TreeSet<PdeEntity> anonymized_dirty = unstructuredTextAnonymizer.extractPdeEntities(plaintextContent);
        Map<Boolean, List<PdeEntity>> collectAnonymized = anonymized_dirty.stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getTextContent())));
        List<PdeEntity> anonymized = collectAnonymized.get(false);
        List<PdeEntity> anonymized_synthetic = collectAnonymized.get(true);

        // calculate overlaps
        Map<BratEntity, List<PdeEntity>> entityMap = entityMatcher.getPdeOverlap(bratEntities, anonymized);
        Map<BratEntity, List<PdeEntity>> entityMap_synthetic = entityMatcher.getPdeOverlap(bratEntities_synthetic, anonymized_synthetic);

        // calculate metrics
        float precision = entityMatcher.getPrecisionPde(entityMap, anonymized);
        float recall = entityMatcher.getRecallPde(entityMap, anonymized);
        float precision_synthetic = entityMatcher.getPrecisionPde(entityMap_synthetic, anonymized_synthetic);
        float recall_synthetic = entityMatcher.getRecallPde(entityMap_synthetic, anonymized_synthetic);

        // generate HTML
        List<NerEntity> flatPdes = anonymized.stream().map(PdeEntity::flatten).collect(Collectors.toList());
        Files.write(html_result,
                htmlGenerator.getHtmlResultPage(bratEntities, flatPdes, plaintextContent,
                        plaintext.getFileName().toString(), precision, recall, 0f).getBytes());

        // filter badly recognized synthetic data
        if (precision_synthetic < 0.99f || recall_synthetic < 0.99f) {
            return new AnnotationResult(0,0,0);
        }
        AnnotationResult annResult = new AnnotationResult(entityMatcher, precision, recall, bratEntities,
                new ArrayList<>(anonymized),
                new ArrayList<>(entityMatcher.getHits(entityMap, anonymized)),
                new ArrayList<>(entityMatcher.getMisses(entityMap, anonymized)),
                false);
        return annResult;
    }

    private int[] countAnnotations(Path ann, UnstructuredTextAnonymizer unstructuredTextAnonymizer) throws IOException {
        final Path plaintext =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".txt"));
        final Path html_result =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".html"));

        String plaintextContent = Files.readString(plaintext).replaceAll("\\r\\n", "\n");

        // get Brat entities
        StandoffFile standoffFile = standoffReader.extractBratEntities(ann, "");
        Map<Boolean, List<BratEntity>> collectBrat = standoffFile.getEntities().stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getText())));
        List<BratEntity> bratEntities  = collectBrat.get(false);
        List<BratEntity> bratEntities_synthetic  = collectBrat.get(true);

        // get PDEs
        TreeSet<PdeEntity> anonymized_dirty = unstructuredTextAnonymizer.extractPdeEntities(plaintextContent);
        Map<Boolean, List<PdeEntity>> collectAnonymized = anonymized_dirty.stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getTextContent())));
        List<PdeEntity> anonymized = collectAnonymized.get(false);
        List<PdeEntity> anonymized_synthetic = collectAnonymized.get(true);

        return new int[] {bratEntities.size(), bratEntities_synthetic.size(), anonymized.size(), anonymized_synthetic.size()};
    }

    private AnnotationResult processAnnFilePdeMatch_Annotators(Path ann, UnstructuredTextAnonymizer unstructuredTextAnonymizer) throws IOException {
        final Path plaintext =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".txt"));

        String plaintextContent = Files.readString(plaintext).replaceAll("\\r\\n", "\n");

        // get PDEs
        TreeSet<PdeEntity> anonymized_dirty = unstructuredTextAnonymizer.extractPdeEntities(plaintextContent);
        Map<Boolean, List<PdeEntity>> collectAnonymized = anonymized_dirty.stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getTextContent())));
        List<PdeEntity> anonymized = collectAnonymized.get(true);


        // get Brat entities
        StandoffFile standoffFile = standoffReader.extractBratEntities(ann, "");
        Map<Boolean, List<BratEntity>> collectBrat = standoffFile.getEntities().stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getText())));
        List<BratEntity> bratEntities = collectBrat.get(true);

        if (anonymized.size() < bratEntities.size()) {
            List<PdeEntity> createed = new ArrayList<>();
            for (BratEntity b: bratEntities) {
                createed.add(new PdeEntity());
            }
            return new AnnotationResult(1f, 1.f,
                    createed, bratEntities, bratEntities);
        }

        // calculate overlaps
        Map<PdeEntity, List<BratEntity>> entityMap = entityMatcher.getBratOverlap(bratEntities, anonymized);

        // calculate metrics
        float precision = entityMatcher.getPrecisionBrat(entityMap, standoffFile.getEntities());
        float recall = entityMatcher.getRecallBrat(entityMap, standoffFile.getEntities());

        AnnotationResult annResult = new AnnotationResult(precision, recall, new ArrayList<>(anonymized),
                bratEntities, new ArrayList<>(entityMatcher.getHitsBrat(entityMap, bratEntities)));
        return annResult;
    }

    private void addEndRule(StringBuilder sb) {
        sb.append("=====================================\n");
    }

    private void addSection(String title, int total, StringBuilder sb) {
        sb.append(title + " ...total: " + total + "\n");
    }

    private void addSection(String title, StringBuilder sb) {
        sb.append(title + "................\n");
    }

}
