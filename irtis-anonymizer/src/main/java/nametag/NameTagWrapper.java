package nametag;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Wrapper for command line NameTag. Deprecated, but might be useful.
 */
@Deprecated
public class NameTagWrapper {
    private static final String NAMETAG_PATH = "nametag-bin-win64/run_ner.exe";
    private static final String ARG_UNTOKENIZED = "--input=untokenized";
    private static final String ARG_OUT = "--output=xml";
    private static final String ARG_MODEL = "libs/czech-cnec2.0-140304.ner";

    /**
     * Produce NER annotated file from given plaintext file
     * @param intermediateFile plaintext
     * @param resultFile NER annotated
     * @throws InterruptedException
     */
    public void produceNametagNerAnnotatedFile(Path intermediateFile, Path resultFile) throws InterruptedException {
        ProcessBuilder builder = getProcessBuilder(
                intermediateFile.toAbsolutePath() + ":" + resultFile.toAbsolutePath());
        Process process = startProcess(builder);
        endNameTag(process);
    }

    private ProcessBuilder getProcessBuilder(String files) {
        return new ProcessBuilder(Paths.get("libs", "nametag-bin-win64", "run_ner.exe").toString(), ARG_UNTOKENIZED, ARG_OUT, ARG_MODEL, files);
    }

    private Process startProcess(ProcessBuilder builder) {
        builder.directory( Paths.get("libs", "nametag-bin-win64").toFile() ); // this is where you set the root folder for the executable to run with
        builder.redirectErrorStream(true);

        try {
            return builder.start();
        } catch (IOException e) {
            System.out.println("Couldn't start the process.");
            e.printStackTrace();
        }
        return null;
    }


    public String runNer(Process nameTag, String text) throws InterruptedException {
        String result = "";
        if (nameTag == null) {
            // stacktrace in stdout
        } else {
            inputText(nameTag, text);
            Scanner s = new Scanner(nameTag.getInputStream());
            result = scanNameTagOutput(nameTag, s); // process dies here
            endNameTag(nameTag);
            s.close();
        }
        return result;
    }

    private void inputText(Process process, String text) {
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
            bw.write(text);
            bw.newLine();
            bw.close();
        } catch (IOException e) {
            System.out.println("Either couldn't read from the template file or couldn't write to the OutputStream.");
            e.printStackTrace();
        }
    }

    private String endNameTag(Process process) throws InterruptedException {
        if (process.isAlive()) {
            int result = process.waitFor();
            return "Process exited with result " + result;
        } else {
            return "Process is not alive.";
        }
    }

    private String scanNameTagOutput(Process process, Scanner s) {
        StringBuilder text = new StringBuilder();
        boolean successCode = false;
        String xml = "";

        while (s.hasNextLine()) {
            String currentLine = s.nextLine();

            if (!successCode && currentLine.contains("Loading ner: done")) {
                successCode = true;
            }
            else if (successCode && !currentLine.startsWith("Recognizing done")) {
                xml += currentLine;
            }

            text.append(currentLine);
            text.append("\n");
        }
        return xml;
    }

    private static String getNameTagPath(String path) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        return classloader.getResource(path).getFile();
    }


}
