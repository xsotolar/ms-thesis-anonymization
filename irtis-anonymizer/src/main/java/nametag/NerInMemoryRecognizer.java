package nametag;

import cz.cuni.mff.ufal.nametag.*;
import gazetteer.exceptions.GazetteerException;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Use NameTag inMemory.
 */
public class NerInMemoryRecognizer {
    public static String NER_LIB_NAME = "czech-cnec2.0-140304.ner";
    public static Path NER_LIB_RELATIVE_PATH = Paths.get("libs");
    private Ner ner;
    private Tokenizer tokenizer;

    /**
     * Constructor. Needs czech-cnec2.0-140304.ner file in the libs dir.
     */
    public NerInMemoryRecognizer() throws GazetteerException {
        ner = Ner.load(NER_LIB_RELATIVE_PATH.resolve(NER_LIB_NAME).toString());
        if (ner == null) {
            throw new GazetteerException("Missing .ner file: " + NER_LIB_NAME);
        }
        tokenizer = ner.newTokenizer();
        if (tokenizer == null) {
            throw new GazetteerException("Tokenizer not loaded!");
        }
    }

    /**
     * Run NER on a line of text.
     * @param text line
     * @return line and list of NER entities
     */
    public ParsedLine parseLine(String text) {
        Forms forms = new Forms();
        TokenRanges tokens = new TokenRanges();
        NamedEntities entities = new NamedEntities();
        ArrayList<NamedEntity> sortedEntities = new ArrayList<>();
        Stack<Integer> openEntities = new Stack<>();
        Stack<NerEntity> nerEntities = new Stack<>();
        List<NerEntity> resultEntities = new ArrayList<>();
        Tokenizer tokenizer = ner.newTokenizer();
        if (tokenizer == null) {
            System.err.println("No tokenizer is defined for the supplied model!");
            System.exit(1);
        }
        StringBuilder builder = new StringBuilder();

        // Tokenize and recognize
        tokenizer.setText(text);
        int unprinted = 0;
        while (tokenizer.nextSentence(forms, tokens)) {
            ner.recognize(forms, entities);
            sortEntities(entities, sortedEntities);

            for (int i = 0, e = 0; i < tokens.size(); i++) {
                TokenRange token = tokens.get(i);
                int token_start = (int) token.getStart();
                int token_end = (int) token.getStart() + (int) token.getLength();

                if (unprinted < token_start) {
                    builder.append(encodeEntities(text.substring(unprinted, token_start)));
                }

                // Open entities starting at current token
                for (; e < sortedEntities.size() && sortedEntities.get(e).getStart() == i; e++) {
                    openEntities.push((int) sortedEntities.get(e).getStart()
                            + (int) sortedEntities.get(e).getLength() - 1);
                    nerEntities.push(new NerEntity(token_start, -1, sortedEntities.get(e).getType()));
                }

                // The token itself
                builder.append(encodeEntities(text.substring(token_start, token_end)));


                // Close entities ending after current token
                while (!openEntities.empty() && openEntities.peek() == i) {
                    openEntities.pop();
                    NerEntity finishedNerEntity = nerEntities.pop();
                    finishedNerEntity.setEnd(token_end);
                    resultEntities.add(finishedNerEntity);
                }
                unprinted = token_end;
            }

            // Write rest of the text (should be just spaces)
            if (unprinted < text.length()) {
                builder.append(encodeEntities(text.substring(unprinted)));
            }
        }
        return new ParsedLine(builder.toString(), resultEntities);
    }

    /**
     * Replace NER entity with given value.
     * @param target
     * @param namedEntity
     * @param newValue if null, use empty string
     * @return if target is null, or the entity is out of bounds, return target.
     */
    public String replaceEntity(String target, NerEntity namedEntity, String newValue) {
        if (target == null
                || namedEntity.getStart() < 0
                || namedEntity.getEnd() >= target.length()) {
            return target;
        }
        if (newValue == null) {
            newValue = "";
        }
        return target.substring(0,
                namedEntity.getStart()) + newValue + target.substring(namedEntity.getEnd());
    }

    private void sortEntities(NamedEntities entities, ArrayList<NamedEntity> sortedEntities) {
        class NamedEntitiesComparator implements Comparator<NamedEntity> {
            public int compare(NamedEntity a, NamedEntity b) {
                if (a.getStart() < b.getStart()) return -1;
                if (a.getStart() > b.getStart()) return 1;
                if (a.getLength() > b.getLength()) return -1;
                if (a.getLength() < b.getLength()) return 1;
                return 0;
            }
        }
        NamedEntitiesComparator comparator = new NamedEntitiesComparator();

        sortedEntities.clear();
        for (int i = 0; i < entities.size(); i++)
            sortedEntities.add(entities.get(i));
        Collections.sort(sortedEntities, comparator);
    }

    private String encodeEntities(String text) {
        //return text.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\"", "&quot;");
        return text;
    }
}
