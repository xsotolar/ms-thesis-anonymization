package nametag;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Run a binary.
 */
@Deprecated
public class ProcessMonitor extends Thread {

    private Process process;
    private int exitCode;

    public ProcessMonitor(Process process) {
        this.process = process;
        start();
    }

    @Override public void run() {
        try {
            exitCode = process.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setOutputStream(final OutputStream s) {
        new Thread(new Runnable() {
            @Override public void run() {
                InputStream is = process.getInputStream();
                int c ;
                try {
                    while((c = is.read()) >= 0) {
                        s.write(c);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void setErrorStream(final OutputStream s) {
        new Thread(new Runnable() {
            @Override public void run() {
                InputStream is = process.getErrorStream();
                int c ;
                try {
                    while((c = is.read()) >= 0) {
                        s.write(c);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public int getExitCode() {
        return exitCode;
    }
}