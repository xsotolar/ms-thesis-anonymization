package nametag;

import anonymization.pde.PdeEntity;
import brat.BratEntity;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Utility class for matching PDE, NER, and Brat entities.
 */
public class EntityMatcher {

    @Deprecated
    public Set<BratEntity> getNaiveMatch(List<BratEntity> bratEntities, List<NerEntity> nerEntities) {
        Set<BratEntity> entityMap = new HashSet<>();
        for (BratEntity be : bratEntities) {
            for (NerEntity ne : nerEntities) {
                if (be.getStartIndex() >= ne.getStart() && be.getEndIndex() <= ne.getEnd()) {
                    entityMap.add(be);
                    break;
                }
            }
        }
        return entityMap;
    }

    /**
     * Combined overlap of all given Ner and Brat anonymization.entities
     * @param bratEntities
     * @param nerEntities
     * @return
     */
    public Map<BratEntity, List<NerEntity>> getCombinedOverlapThreshold(List<BratEntity> bratEntities, List<NerEntity> nerEntities) {
        Map<BratEntity, List<NerEntity>> entityMap = new HashMap<>();
        for (BratEntity be : bratEntities) {
            entityMap.put(be, null);
            List<NerEntity> overlappingNer = new ArrayList<>();
            for (NerEntity ne : nerEntities) {
                int overlap = getEntitiesOverlap(be, ne);
                if (overlap <= 0) {
                    continue;
                }
                overlappingNer.add(ne);
                entityMap.put(be, overlappingNer);
            }
        }
        return entityMap;
    }


    @Deprecated
    public List<NerEntity> filterAllowedClasses(List<NerEntity> entities) {
        return entities.stream()
                .filter(entity -> isAllowedNerClass(entity.getType()))
                .collect(Collectors.toList());
    }

    @Deprecated
    public boolean isAllowedNerClass(String type) {
        String neType = type.toLowerCase();
        return "p".equals(neType)
//                || "pf".equals(neType)
                || "ps".equals(neType)
                || "pm".equals(neType)
//                || "p_".equals(neType)
                || "ah".equals(neType)
                || "at".equals(neType)
                || "az".equals(neType)
                || "gq".equals(neType)
                || "gs".equals(neType)
//                || "gu".equals(neType)
                || "g_".equals(neType)
                || "me".equals(neType)
                || "n_".equals(neType)
                || "regex".equals(neType);
    }

    /**
     * Percentage overlap. 0-100.
     * @param be
     * @param ne
     * @return
     */
    public int getEntitiesOverlap(BratEntity be, NerEntity ne) {
        boolean isOverlap = be.getEndIndex() > ne.getStart() && ne.getEnd() > be.getStartIndex();
        if (!isOverlap) {
            return 0;
        }
        int neStart = ne.getStart();
        int neEnd = ne.getEnd();
        if (ne.getStart() < be.getStartIndex()) {
            neStart = be.getStartIndex();
        }
        if (ne.getEnd() > be.getEndIndex()) {
            neEnd = be.getEndIndex();
        }
        return (int)((((float)neEnd - neStart) / be.getLengthAlphaNumOnly()) * 100);
    }

    /**
     * Ratio of NER anonymization.entities that correctly intersect with PI anonymization.entities.
     * (|PI| intersect |NER|) / |NER|
     */
    public float getPrecision(Map<BratEntity, List<NerEntity>> entityMap, List<NerEntity> retrieved) {
        Set<NerEntity> overlapping = new HashSet<>();
        for (Map.Entry<BratEntity, List<NerEntity>> entry : entityMap.entrySet()) {
            if (entry.getValue() != null) {
                overlapping.addAll(entry.getValue());
            }
        }

        HashSet<NerEntity> retrievedSet = new HashSet<>(retrieved);
        retrievedSet.retainAll(overlapping);

        return (float)retrievedSet.size() / retrieved.size();
    }


    /**
     * Recall is the percentage of correctly identified PI anonymization.entities by NER anonymization.entities.
     * (|PI| intersect |NER|) / |PI|
     */
    public float getRecall(Map<BratEntity, List<NerEntity>> entityMap) {
        long goldCount = entityMap.entrySet().stream()
                .filter(e -> e.getValue() != null)
                .count();

        return (float)goldCount / entityMap.keySet().size();
    }

    /**
     * Recall is the percentage of correctly identified PI anonymization.entities by NER anonymization.entities.
     * (|PI| intersect |NER|) / |PI|
     */
    public float getRecallPde(Map<BratEntity, List<PdeEntity>> entityMap, List<PdeEntity> retrieved) {
        if (entityMap.size() == 0 && retrieved.size() == 0) {
            return 0;
        }
        long truePositives = entityMap.entrySet().stream()
                .filter(e -> e.getValue().size() > 0)
                .count();
        if (truePositives == 0) {
            return 0;
        }
        return (float)truePositives / entityMap.keySet().size();
    }

    /**
     * Recall for annotator performance.
     * @param entityMap
     * @param retrieved
     * @return
     */
    public float getRecallBrat(Map<PdeEntity, List<BratEntity>> entityMap, List<BratEntity> retrieved) {
        if (entityMap.size() == 0 && retrieved.size() == 0) {
            return 0;
        }
        long truePositives = entityMap.entrySet().stream()
                .filter(e -> e.getValue().size() > 0)
                .count();
        if (truePositives == 0) {
            return 0;
        }
        return (float)truePositives / entityMap.keySet().size();
    }

    /**
     * Recall and precision are combined with harmonic mean into F1-measure.
     * 2 * (precision * recall) / (precision + recall)
     */
    public float getFmeasure(Map<BratEntity, List<NerEntity>> entityMap, List<NerEntity> retrieved) {
        float precision = getPrecision(entityMap, retrieved);
        float recall = getRecall(entityMap);

        return (2 * recall * precision) / (recall + precision);
    }

    /**
     * Overlap for natural PDE
     * @param entities
     * @param pdeEntities
     * @return
     */
    public Map<BratEntity, List<PdeEntity>> getPdeOverlap(List<BratEntity> entities, List<PdeEntity> pdeEntities) {
        Map<BratEntity, List<PdeEntity>> result = new HashMap<>(entities.size());
        for (BratEntity bratEntity: entities) {
            result.put(bratEntity, new ArrayList<>());
            for (PdeEntity pdeEntity: pdeEntities) {
                if (isOverlap(bratEntity, pdeEntity)) {
                    result.get(bratEntity).add(pdeEntity);
                }
            }
        }
        return result;
    }

    /**
     * Overlap for brat entities
     * @param entities
     * @param pdeEntities
     * @return
     */
    public Map<PdeEntity, List<BratEntity>> getBratOverlap(List<BratEntity> entities, List<PdeEntity> pdeEntities) {
        Map<PdeEntity, List<BratEntity>> result = new HashMap<>(pdeEntities.size());
        for (PdeEntity pdeEntity: pdeEntities) {
            result.put(pdeEntity, new ArrayList<>());
            for (BratEntity bratEntity: entities) {
                if (isOverlap(bratEntity, pdeEntity)) {
                    result.get(pdeEntity).add(bratEntity);
                }
            }
        }
        return result;
    }

    /**
     * Get PDE precision
     * @param overlapMap
     * @param retrieved
     * @return
     */
    public float getPrecisionPde(Map<BratEntity, List<PdeEntity>> overlapMap, List<PdeEntity> retrieved) {
        // precision doesn't have any weight
        if (overlapMap.size() == 0 && retrieved.size() == 0) {
            return 0;
        }
        Set<PdeEntity> overlappingPde = flattenMapValues(overlapMap);
        HashSet<PdeEntity> intersection = new HashSet<>(retrieved);
        intersection.retainAll(overlappingPde);
        float result;
        if (intersection.size() == 0) {
            result = 0;
        } else if (retrieved.size() == 0) {
            result = 0;
        } else {
            result = (float)intersection.size() / retrieved.size();
        }
        return result;
    }

    /**
     * Get Brat precision
     * @param overlapMap
     * @param retrieved
     * @return
     */
    public float getPrecisionBrat(Map<PdeEntity, List<BratEntity>> overlapMap, List<BratEntity> retrieved) {
        // precision doesn't have any weight
        if (overlapMap.size() == 0 && retrieved.size() == 0) {
            return 0;
        }
        Set<BratEntity> overlappingPde = flattenMapValuesBrat(overlapMap);
        HashSet<BratEntity> intersection = new HashSet<>(retrieved);
        intersection.retainAll(overlappingPde);
        float result;
        if (intersection.size() == 0) {
            result = 0;
        } else if (retrieved.size() == 0) {
            result = 0;
        } else {
            result = (float)intersection.size() / retrieved.size();
        }
        return result;
    }

    /**
     * Add weight by entity count
     * @param precision
     * @param count
     * @return
     */
    public float addWeight(float precision, long count) {
        BigDecimal prec = new BigDecimal(precision);
        BigDecimal zero = new BigDecimal(0);
        if (zero.compareTo(prec) == 0 && count == 0) {
            return 0;
        } else if (zero.compareTo(prec) == 0 || count == 0) {
            return 0;
        } else {
            return precision * count;
        }
    }

    /**
     * Calculate weighted F-measure
     * @param precision
     * @param recall
     * @param size
     * @return
     */
    public float calculateWeightedFmeasure(float precision, float recall, int size) {
        return (precision == 0 || recall == 0 || size == 0)
                ? 0
                : calculateFmeasure(precision, recall) * size;
    }

    /**
     * Calculate pure f-Measure
     * @param precision
     * @param recall
     * @return
     */
    public float calculateFmeasure(float precision, float recall) {
        return (precision == 0 || recall == 0)
                ? 0
                :((2 * recall * precision) / (recall + precision));
    }

    /**
     * Calculate recognition hits
     * @param entityMap
     * @param retrieved
     * @return
     */
    public HashSet<PdeEntity> getHits(Map<BratEntity, List<PdeEntity>> entityMap, List<PdeEntity> retrieved) {
        Set<PdeEntity> overlapping = flattenMapValues(entityMap);
        HashSet<PdeEntity> intersection = new HashSet<>(retrieved);
        intersection.retainAll(overlapping);
        return intersection;
    }

    /**
     * Calculate recognition misses for Brat entities
     * @param entityMap
     * @param retrieved
     * @return
     */
    public HashSet<BratEntity> getHitsBrat(Map<PdeEntity, List<BratEntity>> entityMap, List<BratEntity> retrieved) {
        Set<BratEntity> overlapping = flattenMapValuesBrat(entityMap);
        HashSet<BratEntity> intersection = new HashSet<>(retrieved);
        intersection.retainAll(overlapping);
        return intersection;
    }

    /**
     * Calculate recognition
     * @param entityMap
     * @param retrieved
     * @return
     */
    public HashSet<PdeEntity> getMisses(Map<BratEntity, List<PdeEntity>> entityMap, List<PdeEntity> retrieved) {
        Set<PdeEntity> overlapping = flattenMapValues(entityMap);
        HashSet<PdeEntity> retrievedSet = new HashSet<>(retrieved);
        retrievedSet.removeAll(overlapping);
        return retrievedSet;
    }

    /**
     * Unweigh precision.
     * @param divisor
     * @param divided
     * @return
     */
    public float calculateUnweightedPrecisison(float divisor, float divided) {
        BigDecimal over = new BigDecimal(divisor);
        BigDecimal under = new BigDecimal(divided);
        BigDecimal zero = new BigDecimal(0);
        if (zero.compareTo(over) == 0 && zero.compareTo(under) == 0) {
            return 1f;
        } else if (zero.compareTo(over) == 0 || zero.compareTo(under) == 0) {
            return 0;
        } else {
            return divisor / divided;
        }
    }

    /**
     * Unweigh recall
     * @param divisor
     * @param divided
     * @return
     */
    public float calculateUnweightedRecall(float divisor, int divided) {
        BigDecimal over = new BigDecimal(divisor);
        BigDecimal zero = new BigDecimal(0);
        if (divided == 0) {
            return 1f;
        } else if (zero.compareTo(over) == 0) {
            return 0;
        } else {
            return divisor / divided;
        }
    }

    /**
     * Calculate pure recall
     * @param recall
     * @param size
     * @return
     */
    public float calculateRecall(float recall, int size) {
        BigDecimal under = new BigDecimal(recall);
        BigDecimal zero = new BigDecimal(0);
        if (zero.compareTo(under) == 0 || size == 0) {
            return 0;
        } else {
            return recall * size;
        }
    }

    private Set<PdeEntity> flattenMapValues(Map<BratEntity, List<PdeEntity>> entityMap) {
        Set<PdeEntity> overlapping = new HashSet<>();
        for (Map.Entry<BratEntity, List<PdeEntity>> entry : entityMap.entrySet()) {
            if (entry.getValue() != null) {
                overlapping.addAll(entry.getValue());
            }
        }
        return overlapping;
    }

    private Set<BratEntity> flattenMapValuesBrat(Map<PdeEntity, List<BratEntity>> entityMap) {
        Set<BratEntity> overlapping = new HashSet<>();
        for (Map.Entry<PdeEntity, List<BratEntity>> entry : entityMap.entrySet()) {
            if (entry.getValue() != null) {
                overlapping.addAll(entry.getValue());
            }
        }
        return overlapping;
    }

    private boolean isOverlap(BratEntity bratEntity, PdeEntity pdeEntity) {
        NerEntity pde = pdeEntity.flatten();
        // outside
        if (bratEntity.getStartIndex() > pde.getEnd()) return false;
        if (bratEntity.getEndIndex() < pde.getStart()) return false;

        return true;
    }

    private Set<PdeEntity> flattenMapValuesAndPde(Map<BratEntity, List<PdeEntity>> entityMap) {
        Set<PdeEntity> overlapping = new HashSet<>();
        for (Map.Entry<BratEntity, List<PdeEntity>> entry : entityMap.entrySet()) {
            if (entry.getValue() != null) {
                if (entry.getValue().size() > 1) {
                    PdeEntity created = new PdeEntity(entry.getValue());
                    overlapping.add(created);
                } else {
                    overlapping.add(entry.getValue().get(0));
                }

            }
        }
        return overlapping;
    }


}
