package nametag;

/**
 * POJO for NER tags.
 */
@Deprecated
public class TagIndex {
    private int index;
    private boolean isStart;

    public TagIndex() {
    }

    public TagIndex(int index, boolean isStart) {
        this.index = index;
        this.isStart = isStart;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean start) {
        isStart = start;
    }
}
