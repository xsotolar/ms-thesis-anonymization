package nametag;

import org.ahocorasick.trie.Emit;
import org.ahocorasick.trie.Trie;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Fast multiple string replacer.
 */
@Deprecated
public class StringReplacer {

    private Map<String,String> nerXmlTags;

    private String emojiFilter = "[^\\p{L}\\p{M}\\p{N}\\p{P}\\p{Z}\\p{Cf}\\p{Cs}\\s]";

    public StringReplacer() {
        nerXmlTags = new HashMap<>();
        nerXmlTags.put("&amp;", "&");
        nerXmlTags.put("&lt;", "<");
        nerXmlTags.put("&gt;", ">");
        nerXmlTags.put("&quot;", "\"");
        nerXmlTags.put("&apos;", "'");
        nerXmlTags.put("<token>", "");
        nerXmlTags.put("</token>", "");
        nerXmlTags.put("<sentence>", "");
        nerXmlTags.put("</sentence>", "");
    }

    public String replaceXmlEscape(final String text) {
        return replaceBorAhoCorasick(text, nerXmlTags);
    }

    /**
     * Fast multiple string replace
     * @param text target
     * @param definitions what to replace
     * @return
     */
    public String replaceBorAhoCorasick(final String text, final Map<String, String> definitions) {
        // Create a buffer sufficiently large that re-allocations are minimized.
        final StringBuilder sb = new StringBuilder( text.length() << 1 );

        final Trie.TrieBuilder builder = Trie.builder();
//        builder.onlyWholeWords();
//        builder.removeOverlaps();

        final String[] toReplace = definitions.keySet().toArray(new String[0]);

        for (final String key : toReplace) {
            builder.addKeyword( key );
        }

        final Trie trie = builder.build();
        final Collection<Emit> emits = trie.parseText(text);

        int prevIndex = 0;

        for( final Emit emit : emits ) {
            final int matchIndex = emit.getStart();

            sb.append( text.substring( prevIndex, matchIndex ) );
            sb.append( definitions.get( emit.getKeyword() ) );
            prevIndex = emit.getEnd() + 1;
        }

        // Add the remainder of the string (contains no more matches).
        sb.append( text.substring( prevIndex ) );

        return sb.toString();
    }

    /**
     * Remove XML escape tags from text file
     * @param dirty
     * @param clean
     * @throws IOException
     */
    public void cleanStringFile(Path dirty, Path clean) throws IOException {
        String cleaned = replaceXmlEscape(Files.readString(dirty));
        Files.write(clean, cleaned.getBytes());
    }

    public String cleanStringOfEmojis(String dirty) {
        return dirty.replaceAll(emojiFilter, "");
    }
}
