package nametagtest;

import nametag.StringReplacer;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StringReplacerTest {
    @Test
    public void xmlEscapeReplacement() throws IOException {
        StringReplacer sr = new StringReplacer();

        Path path = Paths.get("src", "test", "resources", "specials.txt");
        String fileContent = sr.replaceXmlEscape(Files.readString(path));

        String specials = "'\"><&";
        Assert.assertEquals(specials, fileContent);
        Assert.assertEquals(specials.length(), fileContent.length());

        Path url = Paths.get("src", "test", "resources", "specials-url.txt");
        Path fixed = Paths.get("src", "test", "resources", "specials-url-fixed.txt");

        String fixedString = sr.replaceXmlEscape(Files.readString(url));
        String expected = Files.readString(fixed);
        Assert.assertEquals(expected, fixedString);
    }
}
