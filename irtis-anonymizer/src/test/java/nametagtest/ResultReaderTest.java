package nametagtest;

import nametag.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ResultReaderTest {
    @Test
    public void parseNer_tokenize() throws NameTagException {
        String line = "Tak termín <ne type=\"T\"><ne type=\"td\">27.</ne><ne type=\"tm\">-29.</ne></ne> si rezervuj prosím";
        String[] expected = {"Tak termín ", "<ne type=\"T\">", "<ne type=\"td\">", "27.", "</ne>", "<ne type=\"tm\">",
                "-29.", "</ne>", "</ne>", " si rezervuj prosím"};

        ResultReader resultReader = new ResultReader();
        String[] actual = resultReader.tokenize(line);

        Assert.assertEquals(expected.length, actual.length);
        for (int i = 0; i < expected.length; i++) {
            Assert.assertEquals(expected[i], actual[i]);
        }
    }

    @Test
    public void parseNer_calcRealTagPosition() throws NameTagException {
        String[] tokens = {"Tak termín ", "<ne type=\"T\">", "<ne type=\"td\">", "27.", "</ne>", "<ne type=\"tm\">",
                "-29.", "</ne>", "</ne>", " si rezervuj prosím"};
        ResultReader resultReader = new ResultReader();

        Assert.assertEquals(11, resultReader.calcTagRealPosition(tokens, 1, 0));
        Assert.assertEquals(11, resultReader.calcTagRealPosition(tokens, 2, 0));
        Assert.assertEquals(14, resultReader.calcTagRealPosition(tokens, 4, 0));
        Assert.assertEquals(14, resultReader.calcTagRealPosition(tokens, 5, 0));

        try {
            resultReader.calcTagRealPosition(tokens, 0, 0);
            Assert.fail();
        } catch (NameTagException e) { /* asserted in try */ }
    }

    @Test
    public void parseNer_findEndTag() throws NameTagException {
        String[] tokens = {"Tak termín ", "<ne type=\"T\">", "<ne type=\"td\">", "27.", "</ne>", "<ne type=\"tm\">",
                "-29.", "</ne>", "</ne>", " si rezervuj prosím"};
        ResultReader resultReader = new ResultReader();

        int nextStartTag = resultReader.findNextStartTag(tokens, 0);
        Assert.assertEquals(1, nextStartTag);
        TagIndex endTag = resultReader.findEndTag(tokens, nextStartTag);
        Assert.assertEquals(8, endTag.getIndex());

        TagIndex endTag2 = resultReader.findEndTag(tokens, 2);
        Assert.assertEquals(4, endTag2.getIndex());

        TagIndex endTag3 = resultReader.findEndTag(tokens, 5);
        Assert.assertEquals(7, endTag3.getIndex());
    }

    @Test
    public void parseNer_findNextStartTag() throws NameTagException {
        String[] tokens = {"Tak termín ", "<ne type=\"T\">", "<ne type=\"td\">", "27.", "</ne>", "<ne type=\"tm\">",
                "-29.", "</ne>", "</ne>", " si rezervuj prosím"};
        ResultReader resultReader = new ResultReader();

        int nextStartTag = resultReader.findNextStartTag(tokens, 0);
        Assert.assertEquals(1, nextStartTag);
    }

    @Test
    public void parseNer_findNextTag() throws NameTagException {
        String[] tokens = {"Tak termín ", "<ne type=\"T\">", "<ne type=\"td\">", "27.", "</ne>", "<ne type=\"tm\">",
                "-29.", "</ne>", "</ne>", " si rezervuj prosím"};
        ResultReader resultReader = new ResultReader();

        TagIndex nextStartTag = resultReader.findNextTag(tokens, 0);
        Assert.assertEquals(1, nextStartTag.getIndex());

        TagIndex t2 = resultReader.findNextTag(tokens, 2);
        Assert.assertEquals(2, t2.getIndex());

        TagIndex t4 = resultReader.findNextTag(tokens, 3);
        Assert.assertEquals(4, t4.getIndex());

        TagIndex end = resultReader.findNextTag(tokens, tokens.length -1);
        Assert.assertNull(end);
    }

    @Test
    public void parseNer_wholeLine_multipleTags() throws NameTagException {
        String line = "Tak termín <ne type=\"T\"><ne type=\"td\">27.</ne><ne type=\"tm\">-29.</ne></ne> si rezervuj prosím";
        String text = "Tak termín 27.-29. si rezervuj prosím";
        String text2 = "<ne type=\"td\">27.</ne>";

        NerEntity e1 = new NerEntity("<ne type=\"T\">", 11, 18);
        NerEntity e2 = new NerEntity("<ne type=\"td\">", 11, 14);
        NerEntity e3 = new NerEntity("<ne type=\"tm\">", 14, 18);

        ResultReader resultReader = new ResultReader();
        ParsedLine parsedLine = resultReader.readNerLine(line, 0);

        Assert.assertEquals(3, parsedLine.getEntities().size());
        Assert.assertTrue(parsedLine.getEntities().contains(e1));
        Assert.assertTrue(parsedLine.getEntities().contains(e2));
        Assert.assertTrue(parsedLine.getEntities().contains(e3));
        Assert.assertEquals(text, parsedLine.getLine());
    }

    @Test
    public void parseNer_wholeLine_simpleTag() throws NameTagException {
        String line = "<ne type=\"td\">27.</ne>";
        NerEntity e1 = new NerEntity("<ne type=\"td\">", 0, 3);

        ResultReader resultReader = new ResultReader();
        ParsedLine parsedLine = resultReader.readNerLine(line, 0);

        Assert.assertEquals(1, parsedLine.getEntities().size());
        Assert.assertTrue(parsedLine.getEntities().contains(e1));

        Assert.assertEquals("27.", parsedLine.getLine());
    }

    @Test
    public void parseNer_wholeLine_noTags() throws NameTagException {
        String line = "27.";

        ResultReader resultReader = new ResultReader();
        ParsedLine parsedLine = resultReader.readNerLine(line, 0);

        Assert.assertEquals(0, parsedLine.getEntities().size());
        Assert.assertEquals("27.", parsedLine.getLine());
    }

    @Test
    public void parseNer_emptyLine() throws NameTagException {
        String line = "";

        ResultReader resultReader = new ResultReader();
        ParsedLine parsedLine = resultReader.readNerLine(line, 0);

        Assert.assertEquals(0, parsedLine.getEntities().size());
        Assert.assertEquals("", parsedLine.getLine());
    }

    @Test
    public void extractEntitiesFromFile() throws NameTagException {
        Path nerFile = Paths.get("src", "test", "resources", "resultfile_valid.txt");
        Path sourceFile = Paths.get("src", "test", "resources", "testfile_valid.txt");
        String fileContent = readLineByLine(sourceFile);

        ResultReader resultReader = new ResultReader();
        NerScanResult nerScanResult = resultReader.readNerFile(nerFile, "");
        List<String> substrings = new ArrayList<>();
        for (NerEntity e : nerScanResult.getNerEntities()) {
            substrings.add(fileContent.substring(e.getStart(), e.getEnd()));
        }
    }

    @Test
    public void extractEntitiesFromFile_oneLine() throws NameTagException, IOException {
        Path nerFile = Paths.get("src", "test", "resources", "newrfile_link.txt");
        Path sourceFile = Paths.get("src", "test", "resources", "newrfile_result.txt");
        String fileContent = Files.readString(sourceFile).replaceAll("\\r\\n", "\n");

        ResultReader resultReader = new ResultReader();
        NerScanResult nerScanResult = resultReader.extractNerEntities(nerFile, "");
        List<String> substrings = new ArrayList<>();
        for (NerEntity e : nerScanResult.getNerEntities()) {
            if (e.getStart() < fileContent.length())  {
                substrings.add(fileContent.substring(e.getStart(), e.getEnd()));
            }
        }
    }

    @Test
    public void extractEntitiesFromFile_oneLine2() throws NameTagException, IOException {
        Path nerFile = Paths.get("src", "test", "resources", "resultfile.txt");
        Path sourceFile = Paths.get("src", "test", "resources", "testfile.txt");
        String fileContent = Files.readString(sourceFile).replaceAll("\\r\\n", "\n");

        ResultReader resultReader = new ResultReader();
        NerScanResult nerScanResult = resultReader.extractNerEntities(nerFile, "");
        List<String> substrings = new ArrayList<>();
        for (NerEntity e : nerScanResult.getNerEntities()) {
            if (e.getStart() < fileContent.length())  {
                substrings.add(fileContent.substring(e.getStart(), e.getEnd()));
            } else {
                Assert.fail("e.getStart() >= fileContent.length()");
            }
        }
    }

    private String readLineByLine(Path filePath) {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(filePath, StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }
}
