package nametagtest;

import brat.BratEntity;
import nametag.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class BratNerMatchTest {

    @Test
    public void testOverlap() throws NameTagException {
        BratEntity be = new BratEntity();
        be.setStartIndex(10);
        be.setEndIndex(20);
        be.setText("aaaaaaaaaa");

        NerEntity ne1 = new NerEntity("<ne type=\"P\">", 0, 5);
        NerEntity ne2 = new NerEntity("<ne type=\"P\">", 0, 10);
        NerEntity ne3 = new NerEntity("<ne type=\"P\">", 5, 15);
        NerEntity ne4 = new NerEntity("<ne type=\"P\">", 15, 17);
        NerEntity ne5 = new NerEntity("<ne type=\"P\">", 15, 25);
        NerEntity ne6 = new NerEntity("<ne type=\"P\">", 20, 30);
        NerEntity ne7 = new NerEntity("<ne type=\"P\">", 25, 35);
        NerEntity ne8 = new NerEntity("<ne type=\"P\">", 5, 35);

        EntityMatcher em = new EntityMatcher();
        Assert.assertTrue(em.getEntitiesOverlap(be, ne1) == 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne2) == 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne3) > 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne4) > 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne5) > 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne6) == 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne7) == 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne8) > 0);
    }

    @Test
    public void testMetrics_PartialOverlap() {
        EntityMatcher entityMatcher = new EntityMatcher();

        BratEntity b1 = new BratEntity();
        b1.setStartIndex(5);
        b1.setEndIndex(15);
        b1.setText("aaaaaaaaaa");
        List<BratEntity> bratEntities = new ArrayList<>();
        bratEntities.add(b1);

        NerEntity n1 = new NerEntity(3,8);
        NerEntity n3 = new NerEntity(9,11);
        NerEntity n4 = new NerEntity(9,14);
        NerEntity n2 = new NerEntity(13,20);
        List<NerEntity> nerEntities = new ArrayList<>();
        nerEntities.add(n1);
        nerEntities.add(n2);
        nerEntities.add(n3);
        nerEntities.add(n4);

        Map<BratEntity, List<NerEntity>> bratEntityListMap =
                entityMatcher.getCombinedOverlapThreshold(bratEntities, nerEntities);

        Assert.assertEquals(1f, entityMatcher.getPrecision(bratEntityListMap, nerEntities), 0.05);
        Assert.assertEquals(1f, entityMatcher.getRecall(bratEntityListMap), 0.05);
        Assert.assertEquals(1f, entityMatcher.getFmeasure(bratEntityListMap, nerEntities), 0.05);
    }

    @Test
    public void testMetrics_NoOverlap() {
        EntityMatcher entityMatcher = new EntityMatcher();

        BratEntity b1 = new BratEntity();
        b1.setStartIndex(5);
        b1.setEndIndex(15);
        b1.setText("aaaaaaaaaa");
        List<BratEntity> bratEntities = new ArrayList<>();
        bratEntities.add(b1);

        NerEntity n1 = new NerEntity(3,5);
        NerEntity n2 = new NerEntity(15,20);
        List<NerEntity> nerEntities = new ArrayList<>();
        nerEntities.add(n1);
        nerEntities.add(n2);

        Map<BratEntity, List<NerEntity>> bratEntityListMap =
                entityMatcher.getCombinedOverlapThreshold(bratEntities, nerEntities);

        Assert.assertEquals(0f, entityMatcher.getPrecision(bratEntityListMap, nerEntities), 0.05);
        Assert.assertEquals(0f, entityMatcher.getRecall(bratEntityListMap), 0.05);
    }

    @Test
    public void testMetrics_PartialMatch() {
        EntityMatcher entityMatcher = new EntityMatcher();

        BratEntity b1 = new BratEntity();
        b1.setStartIndex(5);
        b1.setEndIndex(15);
        b1.setText("aaaaaaaaaa");
        BratEntity b2 = new BratEntity();
        b2.setStartIndex(20);
        b2.setEndIndex(30);
        b2.setText("aaaaaaaaaa");
        List<BratEntity> bratEntities = new ArrayList<>();
        bratEntities.add(b1);
        bratEntities.add(b2);

        NerEntity n4 = new NerEntity(9,14);
        List<NerEntity> nerEntities = new ArrayList<>();
        nerEntities.add(n4);

        Map<BratEntity, List<NerEntity>> bratEntityListMap =
                entityMatcher.getCombinedOverlapThreshold(bratEntities, nerEntities);

        Assert.assertEquals(1f, entityMatcher.getPrecision(bratEntityListMap, nerEntities), 0.05);
        Assert.assertEquals(0.5, entityMatcher.getRecall(bratEntityListMap), 0.05);
        Assert.assertEquals(0.66, entityMatcher.getFmeasure(bratEntityListMap, nerEntities), 0.05);
    }

    @Test
    public void printWeightedResult() {
        int linesOfText = 4541 + 4265 + 639 + 829 + 547;
        int whole = 23 + 3 + 10 + 18 + 8;
        double weighted = ((23*0.83) + 3 + 10  + (18*0.72) + 8) / whole;

        System.out.println(String.format("\nLines of text:\t\t%d\nPI anonymization.entities:\t\t%d\nWeighted recall:\t%.2f\n", linesOfText, whole, weighted));
    }

}
