package anonymizationtest;

import anonymization.CompositeDataAnonymizer;
import anonymization.pde.NamePdeComposer;
import anonymization.pde.PdeComposer;
import anonymization.pde.PdeEntity;
import anonymization.strategies.UnstructuredTextAnonymizer;
import gazetteer.FullNameGazetteer;
import gazetteer.Mode;
import gazetteer.SurnameGazetteer;
import gazetteer.exceptions.GazetteerException;
import nametag.NerEntity;
import nametag.NerInMemoryRecognizer;
import nametag.ParsedLine;
import org.junit.Assert;
import org.junit.Test;
import server.entities.GeneralText;

import java.io.IOException;
import java.util.*;

public class AnonymizationTest {

    @Test
    public void crossModuleTest() throws IOException, GazetteerException {
        SurnameGazetteer surnameGazetteer = new SurnameGazetteer(Mode.BEST_EFFORT);
        String ahoj = surnameGazetteer.getReplacement("Ondro");
        Assert.assertNotNull(ahoj);
        System.out.println(ahoj);
    }

    @Test
    public void useNer() throws GazetteerException, IOException {
        UnstructuredTextAnonymizer unstructuredTextAnonymizer = new UnstructuredTextAnonymizer();
        String actual = unstructuredTextAnonymizer.anonymize("Moje jméno je Ondřej Sotolář.");

        Assert.assertEquals("Moje jméno je XXX.", actual);
    }

    @Test
    public void replaceEntity() throws GazetteerException {
        String target = "Moje jméno je Ondřej Sotolář.";
        NerEntity entity = new NerEntity(14, 28);
        String replacement = "XXX";

        NerInMemoryRecognizer r = new NerInMemoryRecognizer();
        String actual = r.replaceEntity(target, entity, replacement);

        Assert.assertEquals("Moje jméno je XXX.", actual);
    }

    @Test
    public void anonymizeNames() throws GazetteerException, IOException {
        String input = "Moje jméno je Ondřej Sotolář.";
        NerInMemoryRecognizer nerRecognizer = new NerInMemoryRecognizer();
        PdeComposer namePdeComposer = new NamePdeComposer(2,30, 4, 0);

        ParsedLine toAnonymize = nerRecognizer.parseLine(input);
        Set<PdeEntity> foundNames = namePdeComposer.compose(toAnonymize);
        Assert.assertEquals(1, foundNames.size());

        FullNameGazetteer fullNameGazetteer = new FullNameGazetteer(Mode.BEST_EFFORT);
        String actual = input;
        for (PdeEntity name: foundNames) {
            NerEntity flat = name.flatten();
            actual = nerRecognizer.replaceEntity(input, flat, fullNameGazetteer.getReplacement(flat.getContent(input)));
        }
        System.out.println(actual);
    }

    @Test
    public void anonymizeNames2() throws GazetteerException, IOException {
        String input = "Na dovolenou jedu s Dorotou Zelmanovou.";
        NerInMemoryRecognizer nerRecognizer = new NerInMemoryRecognizer();
        PdeComposer namePdeComposer = new NamePdeComposer(2,30, 4, 0);

        ParsedLine toAnonymize = nerRecognizer.parseLine(input);
        TreeSet<PdeEntity> foundNames = namePdeComposer.compose(toAnonymize);
        Assert.assertEquals(1, foundNames.size());

        FullNameGazetteer fullNameGazetteer = new FullNameGazetteer(Mode.BEST_EFFORT);
        String actual = input;
        for (PdeEntity name: foundNames) {
            NerEntity flat = name.flatten();
            actual = nerRecognizer.replaceEntity(input, flat, fullNameGazetteer.getReplacement(flat.getContent(input)));
        }
        System.out.println(actual);
    }

    @Test
    public void anonymizeLine() throws GazetteerException, IOException {
        String[] values = new String[] {
                "Moje jméno je Ondřej Sotolář a jméno mojí ženy je Doris Sotolářová.",
                "Můj kamarád je Jan Novák a moje jméno je Ondřej Sotolář.",
                "Jdu navštívit svého kamaráda Jana Nováka společně s Ondřejem Sotolářem. Můj kamarád je Jan Novák a moje jméno je Ondřej Sotolář. Moje jméno je Ondřej Sotolář a jméno mojí ženy je Doris Sotolářová.",
                "Staré pověsti české je kniha pro mládež Aloise Jiráska. Popisuje události z české historie založené na lidové slovesnosti a některých historických faktech. Poprvé vyšla v roce 1894 v pražském nakladatelství J. R. Vilímek jako 11. svazek v edici Vilímkova knihovna mládeže dospívající. O rok dříve však pověsti vycházely na pokračování ve stejném nakladatelství v sešitech. Knihu Alois Jirásek věnoval Františku Bartošovi. Předlohou byly Hájkova kronika, Kosmova kronika česká a Dalimilova kronika, ale použity byly také Staré letopisy české a mnoho dalších pramenů. Do díla nebyly zapracovány některé známé české pověsti jako o Oldřichu a Boženě nebo Břetislavu a Jitce. Naopak byly zařazeny méně známé židovské, moravské a slovenské pověsti. Při psaní postupoval Jirásek zcela jinak než August Sedláček, který se ve své Sbírce pověstí historických lidu českého v Čechách, na Moravě a ve Slezsku snažil pověsti očistit od pozdějšího umělého literárního zpracování a dobrat se k jádru lidového vyprávění. Jirásek se naopak o „vypravování umělá“ svých předchůdců opírá a sám je bohatě autorsky rozvíjí po svém, a tak vzniká košaté a čtivé líčení se silným vlasteneckým nábojem. S výjimkou období protektorátu vycházely Staré pověsti české opakovaně a často. Oblíbil si ji i gottwaldovský komunistický režim, za nějž vycházely se změnami a doslovem Zdeňka Nejedlého. Moje telefoní číslo je +420 603 323 999.",
                "Mé telefoní číslo je +420 607 033 062, čáslo karty je 2000 1234 1234 1234, číslo účtu je 9876543210/0800, r.č.: 891004/1234 a čop: 9876543210",
                "Můj email je: ahoj@mail.muni.cz a bydl9m v Praze nebo v Brně s na ulici Táborská 897/104 s poštovním číslem 760 01.",
                "Moje ip adresa je 147.123.0.1. 41°24’12.2″N   2°10’26.5″E",
                "Moje GPS adresa je 41 24.2028,  2 10.4418\n",
                "Moje GPS adresa je 41.40338,  2.17403",
                "Jan Amos Komenský byl učitel národů a žil v Brně na Táborské 104. ",
                "Brexit bez dohody se podle něj nedá zakázat, strany mají odlišné představy. Přestože je opozice přesvědčena, že na něj není země připravená, pokud by byly předčasné volby, Johnson má šanci k 31. říjnu odejít podle slibu z EU, dodává Hošek."
        };
        int orderCloseness = 1; int indexCloseness = 30; int minIdentifierLength = 5; int minInputLength = 4;
        int minFirstNames = 0; boolean isUsingRules = true;
        CompositeDataAnonymizer anonymizer = new CompositeDataAnonymizer(orderCloseness, indexCloseness, minIdentifierLength, minInputLength, minFirstNames, isUsingRules);
        GeneralText data = new GeneralText();
        for (String s: values) {
            data.setText(s);
            anonymizer.anonymize(data);
            System.out.println(data.getText());
        }
    }

}
