package gazetteer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * City gazetteer.
 */
public class CityGazetteer extends BendableNameGazetteer {

    /**
     * Constructor.
     * @param mode specifies what happens when operation doesn't succeed
     * @throws IOException
     */
    public CityGazetteer(Mode mode) throws IOException {
        loadGazetteer(getGazetteerFileName(true), getGazetteerSaltPath(true));
        this.mode = mode;
    }

    /**
     * Get salt path.
     * @param isThrowing throw exception if the salt file is missing
     * @return
     */
    public static Path getGazetteerSaltPath(boolean isThrowing) {
        Path potential = Paths.get(System.getProperty("user.dir"))
                .resolve(GAZETTEER_FOLDER).resolve("city" + FILE_EXTENSION_SALT);
        if (!Files.exists(potential) && isThrowing) {
            throw new IllegalStateException("Salt doesn't exist on path: " + potential);
        }
        return potential;
    }

    /**
     * Get data file path.
     * @param isThrowing throw exception if the data file is missing
     * @return
     */
    public static Path getGazetteerFileName(boolean isThrowing) {
        Path potential = Paths.get(System.getProperty("user.dir"))
                .resolve(GAZETTEER_FOLDER).resolve("city" + FILE_EXTENSION_TXT);
        if (!Files.exists(potential) && isThrowing) {
            throw new IllegalStateException("Gazetteer doesn't exist on path: " + potential);
        }
        return potential;
    }
}
