package gazetteer;

import morphology.MorphoTagger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Base class for all gazetteers. Handles the data file loading and access.
 */
public class GazetteerBase {
    public final static String FILE_EXTENSION_TXT = ".txt";
    public final static String FILE_EXTENSION_SALT = ".salt";
    public final static String GAZETTEER_FOLDER = "gazetteers";

    protected String[] gazetteer;
    protected byte[] salt;
    protected MorphoTagger tagger = new MorphoTagger();

    protected Mode mode;
    protected final int MAX_GAZETTEER_SEARCH = 100;

    /**
     * Get gazetteer size.
     * @return gazetteer size
     */
    public int getSize() {
        return gazetteer.length;
    }

    /**
     * Get gazetteer entry by index.
     * @param index index
     * @return entry on index
     */
    public String getEntry(int index) {
        if (index < 0 || index >= gazetteer.length) {
            throw new IllegalArgumentException("Index '" + index +
                    "' is larger than the gazetteer length '" + getSize() + "'!");
        }
        return gazetteer[index];
    }

    /**
     * Check if input is null or empty. Trim if specified.
     * @param name name
     * @param isTrim trim result
     * @return checked input
     */
    protected String validateNameInput(String name, boolean isTrim) {
        if (name == null || "".equals(name)) {
            throw new IllegalArgumentException("Value is empty: " + name);
        }
        return isTrim ? name.trim() : name;
    }

    protected void loadGazetteer(Path path, Path saltPath) throws IOException {
        gazetteer = Files.lines(path, StandardCharsets.UTF_8).toArray(String[]::new);
        salt = Files.readAllBytes(saltPath);
    }

    /**
     * Get gazetteer value by hash of the input and salt. Returns allways the same value for equal inputs.
     * @param input input
     * @return gazetteer value
     */
    public String getByHash(String input) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Wrong hashing algorithm name!");
        }
        md.update(salt);
        byte[] hashedPassword = md.digest(input.getBytes(StandardCharsets.UTF_8));
        int hashCode = Arrays.hashCode(hashedPassword);
        int modHash = hashCode % getSize();
        if (modHash < 0)  {
            modHash += getSize();
        }
        return getEntry(modHash);
    }

    public String capitalize(String input) {
        if (input == null || input.length() < 1) {
            return input;
        }
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }


}
