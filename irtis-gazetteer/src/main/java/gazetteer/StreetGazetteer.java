package gazetteer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Street gazetteer.
 */
public class StreetGazetteer extends FixedValueGazetteer {

    /**
     * Constructor.
     * @throws IOException
     */
    public StreetGazetteer() throws IOException {
        loadGazetteer(getGazetteerFileName(true), getGazetteerSaltPath(true));
    }

    /**
     * Get salt path.
     * @param isThrowing throw exception if the salt file is missing
     * @return
     */
    public static Path getGazetteerSaltPath(boolean isThrowing) {
        Path potential = Paths.get(System.getProperty("user.dir"))
                .resolve(GAZETTEER_FOLDER).resolve("street" + FILE_EXTENSION_SALT);
        if (!Files.exists(potential) && isThrowing) {
            throw new IllegalStateException("Salt doesn't exist on path: " + potential);
        }
        return potential;
    }

    /**
     * Get data file path.
     * @param isThrowing throw exception if the data file is missing
     * @return
     */
    public static Path getGazetteerFileName(boolean isThrowing) {
        Path potential = Paths.get(System.getProperty("user.dir"))
                .resolve(GAZETTEER_FOLDER).resolve("street" + FILE_EXTENSION_TXT);
        if (!Files.exists(potential) && isThrowing) {
            throw new IllegalStateException("Gazetteer doesn't exist on path: " + potential);
        }
        return potential;
    }
}
