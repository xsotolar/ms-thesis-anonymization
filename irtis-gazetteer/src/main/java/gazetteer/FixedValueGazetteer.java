package gazetteer;

/**
 * Gazetteer base for fixed form inputs.
 */
public class FixedValueGazetteer extends GazetteerBase implements StringValueGazetteer {
    /**
     * Get replacement by hash
     * @param name
     * @return
     */
    @Override
    public String getReplacement(String name) {
        String input = super.validateNameInput(name, true);
        return capitalize(getByHash(input));
    }
}
