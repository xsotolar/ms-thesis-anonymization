package gazetteer;

import gazetteer.exceptions.GazetteerException;

public interface StringValueGazetteer {
    String getReplacement(String name) throws GazetteerException;
}
