package gazetteer;

public enum Mode {
    THROW, RETURN_ORIGINAL, BEST_EFFORT
}
