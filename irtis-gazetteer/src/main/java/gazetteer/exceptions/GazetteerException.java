package gazetteer.exceptions;

public class GazetteerException extends Exception {
    public GazetteerException(String message) {
        super(message);
    }

    public GazetteerException(String message, Throwable cause) {
        super(message, cause);
    }
}
