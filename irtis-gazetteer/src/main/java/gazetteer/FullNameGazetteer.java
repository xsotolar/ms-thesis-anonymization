package gazetteer;

import gazetteer.exceptions.GazetteerException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Full name gazetteer.
 */
public class FullNameGazetteer extends GazetteerBase implements StringValueGazetteer {
    private FirstNameGazetteer firstNameGazetteer;
    private SurnameGazetteer surnameGazetteer;

    /**
     * Constructor.
     * @param mode specifies what happens when operation doesn't succeed
     * @throws IOException
     */
    public FullNameGazetteer(Mode mode) throws IOException {
        firstNameGazetteer = new FirstNameGazetteer(mode);
        surnameGazetteer = new SurnameGazetteer(mode);
    }

    /**
     *
     * @param name
     * @return
     * @throws GazetteerException
     */
    @Override
    public String getReplacement(String name) throws GazetteerException {
        String input = validateNameInput(name, true);
        String[] split = input.split("\\s+");
        if (split.length < 2) {
            return surnameGazetteer.getReplacement(split[0]);
        }
        else {
            List<String> firstNames = new ArrayList<>(split.length - 1);
            for (int i = 0; i < split.length - 1; i++) {
                firstNames.add(firstNameGazetteer.getReplacement(split[i]));
            }
            String surname = surnameGazetteer.getReplacement(split[split.length - 1]);
            return String.join(" ", String.join(" ", firstNames), surname);
        }
    }
}
