package gazetteer;

import gazetteer.exceptions.GazetteerException;
import morphology.WordForm;

import java.util.List;

/**
 * Gazetteer base from bendable words.
 */
public class BendableNameGazetteer extends GazetteerBase implements StringValueGazetteer {
    /**
     * Generate a random replacement for a given surname. The result is generated from a list of all Czech surnames
     * and respects the input's form if possible; if not, a lemma is used instead.
     * @param name surname
     * @return replacement surname
     */
    @Override
    public String getReplacement(String name) throws GazetteerException {
        String input = super.validateNameInput(name, true);
        try {
            WordForm wordForm = getLemma(input);
            String byHash = getReplacementByHash(wordForm, MAX_GAZETTEER_SEARCH);
            return capitalize(byHash);
        }
        catch (GazetteerException g) {
            switch (mode) {
                case BEST_EFFORT:
                    return getByHash(name);
                case RETURN_ORIGINAL:
                    return name;
                case THROW:
                default:
                    throw g;
            }
        }
    }

    /**
     * Tag the input. If more tokens are found, return the first.
     * @param surname
     * @return
     * @throws GazetteerException
     */
    private WordForm getLemma(String surname) throws GazetteerException {
        List<WordForm> forms = tagger.tagSentence(surname);
        WordForm toAnonymize = null;
        if (forms != null && forms.size() > 0) {
            toAnonymize = forms.get(0);
        } else {
            throw new GazetteerException("Sentence couldn't be tagged: " + surname);
        }
        return toAnonymize;
    }

    private String getReplacementByHash(WordForm surname, int maxTries) throws GazetteerException {
        WordForm byHash;
        for (int i = 0; i < maxTries; i++) {
            String retrieved = getByHash(surname.getLemma() + i);
            List<WordForm> gazetteerValue = tagger.tagSentence(retrieved);

            if (gazetteerValue != null && gazetteerValue.size() > 0) {
                if (surname.getGender() == gazetteerValue.get(0).getGender()) {
                    byHash = gazetteerValue.get(0);

                    List<WordForm> newForms = tagger.generateForm(byHash.getLemma(), surname.getTag());
                    for (WordForm anonymized: newForms) {
                        if (surname.getTag().equals(anonymized.getTag())) {
                            return anonymized.getLemma();
                        }
                    }
                }
            }
        }
        throw new GazetteerException(
                "Gazetteer replacement couldn't be found after: " + maxTries + " tries for: " +
                        surname.getLemma() + ", tag: " + surname.getTag());
    }
}
