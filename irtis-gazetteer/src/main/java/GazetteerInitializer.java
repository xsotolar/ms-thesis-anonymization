import gazetteer.CityGazetteer;
import gazetteer.FirstNameGazetteer;
import gazetteer.StreetGazetteer;
import gazetteer.SurnameGazetteer;
import gazetteer.exceptions.GazetteerException;
import webscrapper.GazetteerScraper;

import java.io.*;
import java.nio.file.Files;
import java.security.SecureRandom;

/**
 * Create gazetteer datafiles and salt values. If the salts are rewritten, the new hash values won't match the old ones.
 */
public class GazetteerInitializer {
    public static void initAll() throws IOException, GazetteerException {
        initSurnameGazetteer();
        initFirstNameGazetteer();
        initStreetNameGazetteer();
        initCityGazetteer();
    }

    private static void initSurnameGazetteer() throws IOException {
        if (!Files.exists(SurnameGazetteer.getGazetteerFileName(false))) {
            GazetteerScraper s = new GazetteerScraper();
            s.saveSurnamesToProjectDir();
        }
        if (!Files.exists(SurnameGazetteer.getGazetteerSaltPath(false))) {
            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[16];
            random.nextBytes(salt);
            Files.write(SurnameGazetteer.getGazetteerSaltPath(false), salt);
        }
    }

    private static void initFirstNameGazetteer() throws IOException {
        if (!Files.exists(FirstNameGazetteer.getGazetteerFileName(false))) {
            GazetteerScraper s = new GazetteerScraper();
            s.saveFirstNamesToProjectDir();
        }
        if (!Files.exists(FirstNameGazetteer.getGazetteerSaltPath(false))) {
            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[16];
            random.nextBytes(salt);
            Files.write(FirstNameGazetteer.getGazetteerSaltPath(false), salt);
        }
    }

    private static void initStreetNameGazetteer() throws GazetteerException, IOException {
        if (!Files.exists(StreetGazetteer.getGazetteerFileName(false))) {
            throw new GazetteerException("No streetname gazetter on path: " +
                    StreetGazetteer.getGazetteerFileName(false));
        }
        if (!Files.exists(StreetGazetteer.getGazetteerSaltPath(false))) {
            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[16];
            random.nextBytes(salt);
            Files.write(StreetGazetteer.getGazetteerSaltPath(false), salt);
        }
    }

    private static void initCityGazetteer() throws GazetteerException, IOException {
        if (!Files.exists(CityGazetteer.getGazetteerFileName(false))) {
            throw new GazetteerException("No streetname gazetter on path: " +
                    CityGazetteer.getGazetteerFileName(false));
        }
        if (!Files.exists(CityGazetteer.getGazetteerSaltPath(false))) {
            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[16];
            random.nextBytes(salt);
            Files.write(CityGazetteer.getGazetteerSaltPath(false), salt);
        }
    }
}
