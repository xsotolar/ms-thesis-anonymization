package morphology;

import cz.cuni.mff.ufal.morphodita.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for tagging word forms. Needs a MorphoDiTa model (.tagger).
 * The dafault model is libs\czech-morfflex-pdt-161115.tagger
 */
public class MorphoTagger {
    private static String DEFAULT_TAGGER = "libs\\czech-morfflex-pdt-161115.tagger";
    private Morpho morpho;

    private Tagger tagger;
    private Tokenizer tokenizer;

    public MorphoTagger() {
        this(DEFAULT_TAGGER);
    }

    /**
     * Constructor. Requires the .tagger model path for MorphoDiTa.
     * @param taggerPath .tagger model
     */
    public MorphoTagger(String taggerPath) {
        tagger = Tagger.load(taggerPath);
        if (tagger == null) {
            throw new IllegalArgumentException("Cannot load tagger from file '" + taggerPath + "'");
        }
        morpho = tagger.getMorpho();
        tokenizer = tagger.newTokenizer();
        if (tokenizer == null) {
            throw new IllegalArgumentException("No tokenizer is defined for the supplied model!");
        }
    }

    /**
     * Tag sentence.
     * @param text input text
     * @return tagged tokens
     */
    public List<WordForm> tagSentence(String text) {
        Forms forms = new Forms();
        TaggedLemmas lemmas = new TaggedLemmas();
        TokenRanges tokens = new TokenRanges();
        List<WordForm> result = new ArrayList<>();

        tokenizer.setText(text);
        while (tokenizer.nextSentence(forms, tokens)) {
            tagger.tag(forms, lemmas);

            for (int i = 0; i < lemmas.size(); i++) {
                TaggedLemma lemma = lemmas.get(i);
                result.add(new WordForm(
                        encodeEntities(lemma.getLemma()),
                        encodeEntities(lemma.getTag())));
            }
        }
        return result;
    }

    /**
     * Generate word form by given tag.
     * @param lemma word lemma
     * @param formTag form tag
     * @return generated forms
     */
    public List<WordForm> generateForm(String lemma, String formTag) {
        TaggedLemmasForms tg = new TaggedLemmasForms();
        morpho.generate(lemma, formTag, 0, tg);
        List<WordForm> result = new ArrayList<>();

        for (int i = 0; i < tg.size(); i++) {
            TaggedLemmaForms taggedLemmaForms = tg.get(0);
            TaggedForms forms = taggedLemmaForms.getForms();
            TaggedForm taggedForm = forms.get(0);

            String tag = taggedForm.getTag();
            String form = taggedForm.getForm();
            result.add(new WordForm(form, tag));
        }
        return result;
    }

    private String encodeEntities(String text) {
        return text
                .replaceAll("&", "&amp;")
                .replaceAll("<", "&lt;")
                .replaceAll(">", "&gt;")
                .replaceAll("\"", "&quot;")
                .replaceAll("_.*", "");
    }



}
