package morphology;

/**
 * Data class for word forms.
 */
public class WordForm {
    public static final int TAG_FORM = 4;
    public static final int TAG_GENDER = 2;
    private static final String QIESTIONMARK = "?";

    private String lemma;
    private String tag;

    public WordForm(String lemma, String tag) {
        this.lemma = lemma;
        this.tag = tag;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        throw new IllegalStateException();
    }

    /**
     * Replaces the given positions in the form with questionmarks.
     * @param positions
     * @return
     */
    public String getRelativizedForm(int... positions) {
        String result = tag;
        for (int i: positions) {
            if (i >= 0 && i < 15) {
                result = result.substring(0,i) + QIESTIONMARK + (i + 1 < result.length() ? result.substring(i + 1) : "");
            }
        }
        return result;
    }

    public char getGender() {
        if (tag.length() != 15) {
            throw new IllegalStateException("Word form is not 15 chars long!");
        }
        return tag.charAt(TAG_GENDER);
    }


}
