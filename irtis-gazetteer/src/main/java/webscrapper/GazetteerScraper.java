package webscrapper;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import gazetteer.FirstNameGazetteer;
import gazetteer.SurnameGazetteer;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Get surname and name data files for gazetteers.
 */
public class GazetteerScraper {

    private String alphabet = "abcdefghijklmnopqrstuvwxyz";

    /**
     * Create surname gazetteer data file from /www.kdejsme.cz/seznam/prijmeni/
     * @throws IOException
     */
    public void saveSurnamesToProjectDir() throws IOException {
        String urlBase = "https://www.kdejsme.cz/seznam/prijmeni/";
        String nameList = scrapeKdejsme(urlBase);
        Files.writeString(
                SurnameGazetteer.getGazetteerFileName(false), nameList, StandardCharsets.UTF_8);
    }

    /**
     * Create first name gazetteer data file from /www.kdejsme.cz/seznam/jmeno/
     * @throws IOException
     */
    public void saveFirstNamesToProjectDir() throws IOException {
        String urlBase = "https://www.kdejsme.cz/seznam/jmeno/";
        String nameList = scrapeKdejsme(urlBase);
        Files.writeString(
                FirstNameGazetteer.getGazetteerFileName(false), nameList, StandardCharsets.UTF_8);
    }

    private String scrapeKdejsme(String urlBase) throws IOException {
        WebClient client = new WebClient();
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(false);
        StringBuilder stringBuilder = new StringBuilder();
        for (String letter: alphabet.split("")) {
            WebRequest webRequest = new WebRequest(new URL(urlBase + letter));
            webRequest.setCharset("utf-8");
            HtmlPage page = client.getPage(webRequest);
            List<HtmlElement> items = (List<HtmlElement>) page.getByXPath("//ul[@class=\"longList\"]//li");
            Stream<String> stringStream = items.stream()
                    .map(x -> x.getFirstChild().asText().replace("\"", ""))
                    .filter(x -> !x.contains(" "))
                    .filter(x -> !x.contains("."))
                    .filter(x -> !x.contains("?"));
            String names = stringStream.collect(Collectors.joining("\n"));
            stringBuilder.append(names);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString().trim();
    }
}
