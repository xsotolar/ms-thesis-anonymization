package logger;

public class Logger {
    public static void info(String message) {
        System.out.println(message);
    }

    public static void info(String message, String value) {
        System.out.println(message + value);
    }
}
