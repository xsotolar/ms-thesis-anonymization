import gazetteer.*;
import gazetteer.exceptions.GazetteerException;
import morphology.MorphoTagger;
import morphology.WordForm;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class GazetteerTest {

    @Test
    public void getByHash_NotNull() throws IOException {
        SurnameGazetteer surnameGazetteer = new SurnameGazetteer(Mode.THROW);
        String surname = "Pálfy";
        assertNotNull(surnameGazetteer.getByHash(surname));
    }

    @Test
    public void tryTagger() throws IOException {
        MorphoTagger tagger = new MorphoTagger();
        List<WordForm> forms = tagger.tagSentence("Novak");
        System.out.println(forms.stream()
                .map(WordForm::getLemma)
                .collect(Collectors.joining(",")));

        List<WordForm> all = new ArrayList<>(10000);
        SurnameGazetteer surnameGazetteer = new SurnameGazetteer(Mode.THROW);
        for (int i = 0; i < 10000; i++) {
            String entry = surnameGazetteer.getEntry(i);
            List<WordForm> forms2 = tagger.tagSentence(entry);
            all.addAll(forms2);
        }
        System.out.println(all.get(500).getLemma());
        System.out.println(all.get(5000).getLemma());
        System.out.println(all.size());
    }

    @Test
    public void anonymize() throws IOException, GazetteerException {
        SurnameGazetteer surnameGazetteer = new SurnameGazetteer(Mode.BEST_EFFORT);
        String[] surnames = new String[] {
                "Novák",
                "Nováka",
                "Novákovi",
                "Nováka",
                "Nováku",
                "Novákovi",
                "Novákem"};

        for (String s: surnames) {
            String replacement = surnameGazetteer.getReplacement(s);
            System.out.println(replacement);
        }
        String[] femaleSurnames = new String[] {
                "Zelmanová",
                "Zelmanové",
                "Zelmanové",
                "Zelmanové",
                "Zelmanová",
                "Zelmanové",
                "Zelmanovou",
                "Sotolářová",
                "Sotolářové",
                "Sotolářové",
                "Sotolářové",
                "Sotolářová",
                "Sotolářové",
                "Sotolářovou"};
        for (String s: femaleSurnames) {
            String replacement = surnameGazetteer.getReplacement(s);
            System.out.println(replacement);
        }
    }

    @Test
    public void anonymizeFirstNames() throws IOException, GazetteerException {
        FirstNameGazetteer firstNameGazetteer = new FirstNameGazetteer(Mode.BEST_EFFORT);
        String[] firstNames = new String[]{
                "Ondřej",
                "Ondřeje",
                "Ondřejovi",
                "Ondřeji",
                "Ondřeji",
                "Ondřejovi",
                "Ondřejem"};

        for (String s : firstNames) {
            String replacement = firstNameGazetteer.getReplacement(s);
            System.out.println(replacement);
        }
        String[] firstNames2 = new String[]{
                "Jan",
                "Jana",
                "Janovi",
                "Janu",
                "Jane",
                "Janovi",
                "Janem"};
        for (String s : firstNames2) {
            String replacement = firstNameGazetteer.getReplacement(s);
            System.out.println(replacement);
        }
    }

    @Test
    public void anonymizeFullNames() throws IOException, GazetteerException {
        FullNameGazetteer fullNameGazetteer = new FullNameGazetteer(Mode.BEST_EFFORT);
        String[] firstNames = new String[]{
                "Ondřej Sotolář",
                "Ondřeje Sotoláře",
                "Ondřejovi Sotolářovi",
                "Ondřeji Sotoláři",
                "Ondřeji Sotoláři",
                "Ondřejovi Sotolářovi",
                "Ondřejem Sotolářem"};
        for (String s : firstNames) {
            String replacement = fullNameGazetteer.getReplacement(s);
            System.out.println(replacement);
        }
        String[] firstNames2 = new String[]{
                "Jan Novák",
                "Jana Nováka",
                "Janem Novákem",
                "Janu Nováku",
                "Jane Nováku",
                "Janovi Novákovi",
                "Janem Novákem"};
        for (String s : firstNames2) {
            String replacement = fullNameGazetteer.getReplacement(s);
            System.out.println(replacement);
        }
    }

    @Test
    public void testRelativize() {
        WordForm empty = new WordForm("","");
        WordForm one = new WordForm("","1");
        WordForm last = new WordForm("","123");

        assertEquals("", empty.getRelativizedForm(new int[] {}));
        assertEquals("?", one.getRelativizedForm(new int[] {0}));
        assertEquals("12?", last.getRelativizedForm(new int[] {2}));
        assertEquals("123", last.getTag());
        assertEquals("???", last.getRelativizedForm(new int[] {0,1,2}));
    }

    @Test
    public void anonymizeStreetNames() throws IOException, GazetteerException {
        StreetGazetteer streetGazetteer = new StreetGazetteer();
        String[] firstNames = new String[]{
                "Táborská",
                "Botanická",
                "Bří. Jaroňků",
                "Táborsk",
                "Táborská"};
        for (String s : firstNames) {
            String replacement = streetGazetteer.getReplacement(s);
            System.out.println(replacement);
        }
    }

    @Test
    public void anonymizeCity() throws IOException, GazetteerException {
        CityGazetteer cityGazetteer = new CityGazetteer(Mode.BEST_EFFORT);
        String[] firstNames = new String[]{
                "Brno",
                "Brna",
                "Brnu",
                "Brno",
                "Brno",
                "Brnu",};
        for (String s : firstNames) {
            String replacement = cityGazetteer.getReplacement(s);
            System.out.println(replacement);
        }
        String[] firstNames2 = new String[]{
                "Praha",
                "Prahy",
                "Praze",
                "Prahu",
                "Praho",
                "Prahou",};
        for (String s : firstNames2) {
            String replacement = cityGazetteer.getReplacement(s);
            System.out.println(replacement);
        }
    }

    @Test
    public void capitalize() {
        GazetteerBase g = new GazetteerBase();

        assertNull(g.capitalize(null));
        assertEquals("", g.capitalize(""));
        assertEquals("A", g.capitalize("a"));
        assertEquals("Ab", g.capitalize("ab"));
    }
}
